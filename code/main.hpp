#ifndef _MAIN_H_
#define _MAIN_H_


#include <cmath>
#include <vector>
#include <iostream>
#include <cstdlib> 
#include <cstring>
#include <limits>

#include "vector_operators.hpp"
#include "solve_les.hpp"
#include "ode_solve.hpp"
#include "functional.hpp"
#include "method_tests.hpp"
#include "tasks.hpp"

using namespace std;

/*
  function headers
*/
void make_run();
void usage_print_out(char *);

#endif /* _MAIN_H_ */

