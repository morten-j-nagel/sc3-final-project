//====================================================================
//                
// Filename:      method_tests.cpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Wed May 29 11:21:02 2013
// Modified at:   Fri Jun  7 15:40:16 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   various test-cases to evaluating the shooting
//                methods.
// Update count:  0
//                
//====================================================================

#include "method_tests.hpp"

// select wished test
void select_run_test(char *prog_name, int n)
{
  switch (n)
    {
    case 0: runtest1(); runtest2(); runtest3(); runtest4(); runtest5();
      break;
    case 1: runtest1();
      break;
    case 2: runtest2();
      break;
    case 3: runtest3();
      break;
    case 4: runtest4();
      break;
    case 5: runtest5();
      break;
    default: print_out_run_test_info(prog_name);
    }
  
  return;
}

// some output for user guidance

void print_out_run_test_info(char *prog_name)
{
  cout << "Run with '" << prog_name << " -t N' to compute following tests: "<< endl
       << "  0: selects all tests" << endl
       << "  1: solves 'y\" = 3/2 y^2; y(0) = 4; y(1) = 1' " << endl
       << "     (testing single shooting method [SSM] )" << endl
       << "  2: solves 'y\" = -exp(-xy) - sin(y'); y(1) = 0; y(2) = 0' " << endl
       << "     (testing multiple shooting method [MSM] )" << endl
       << "  3: solves 'y\" = 5.0*sinh(5*y) y(0) = 0; y(1) = 1' " << endl
       << "     (testing MSM node generation during computation)" << endl
       << "  4: solves 'y\" = -λy; y(0) = 0; y(1) = 1; ∫_0^1 y^2 dx = 1' " << endl
       << "     (testing to force a solution on the system)" << endl
       << "  5: solves Schroedinger Eq. for inf. well with deformed ground" << endl
       << "     (testing a solution for a different system as estimator)" << endl
       << endl
       << "WARNING: Tests only check for execute success - no validation of results!"
       << endl << endl;
}

/*
  first test: solve y" = 3/2 y^2; y(0) = 4; y(1) = 1;
  (system has two solutions...)
*/

void runtest1()
{
  cout << "============================================================" << endl
       << "Computing test 1 ... " <<endl;
  
  vector < double > f,x;

  FUNCTIONAL funcs(2);

  // take the simple single shooting method
  funcs.set_single_shooting();
  
  // set estimator parabola
  x.push_back(0.0);
  x.push_back(1.0);
  f.push_back(4.0);   // f (0)
  f.push_back(-10.0); // f'(0)
  f.push_back(1.0);   // f (1)

  funcs.estimator.set_parabol(f,x);
  
  // choose system
  funcs.r(test_rf1);
  funcs.f(test_f1);

  // set limits and set intital guess from estimator
  funcs.xk = x;
  funcs.sk_start = funcs.estimator.get_starting_guess(&x,2);

  // double number of broyden to newton steps
  funcs.set_broyden_steps(10);
  
  // find first solution
  funcs.print_iterations_to_file("data/single_test-iter.dat");

  funcs.solve_les();

  funcs.print_run_information();

  funcs.print_result_to_file("data/single_test.dat",true);
  /*
    find second solution:
      (in principle one could decrease slowly the slope of the guessing
       parabola in x0=0, but this leads to instabilities in the inter-
       mediate region - therefore here as a test only one certain value)
  */
  
  f.at(1) = -50.0;
  funcs.estimator.set_parabol(f,x);
  funcs.sk_start = funcs.estimator.get_starting_guess(&x,2);
  funcs.xk = x;
  funcs.finished = false;
  
  funcs.solve_les();

  funcs.print_run_information();

  funcs.print_result_to_file("data/single_test.dat",true);

  cout << "SUCCESS!" << endl
       << "Check: 'data/single_test{-iter}.dat' for generate solutions"
       << endl << endl;
}


vector <double> test_f1(double *x,vector <double> *y) 
{
  vector <double> result(*y);
  result.at(0) = 3.0/2.0*pow(y->at(1),2);
  result.at(1) = y->at(0);
    
  return result;
}


vector <double> test_rf1(vector <double> *ya,
		   vector <double> *yb) 
{
  vector <double> result;
  result.push_back(ya->at(1) - 4.0);
  result.push_back(yb->at(1) - 1.0);
  return result;
}


/*
  second test: solve y" = -exp(-xy) - sin(y');
  y(1) = 0; y(2) = 0;
  (from lector notes - simple case to test multiple shooting method)
*/

void runtest2()
{
  cout << "============================================================" << endl
       << "Computing test 2 ... " <<endl;
  
  vector < double > f,x;

  FUNCTIONAL funcs(2);

  // set estimator parabola
  x.push_back(1.0);
  x.push_back(2.0);
  f.push_back(0.2);
  f.push_back(0.0);
  f.push_back(0.2);

  funcs.estimator.set_parabol(f,x);

  // choose system
  funcs.r(test_rf2);
  funcs.f(test_f2);

  // add nodes
  x.insert(x.begin()+1,1.75);
  x.insert(x.begin()+1,1.5);
  x.insert(x.begin()+1,1.25);
  funcs.grow_xk = 0; // do not add additional nodes during computation
  
  // set limits and set intital guess from estimator
  funcs.xk = x;
  funcs.sk_start = funcs.estimator.get_starting_guess(&x,2);
  
  // find solution
  funcs.print_iterations_to_file("data/lecture-system-iter.dat");

  funcs.solve_les();

  funcs.print_run_information();

  cout << "SUCCESS!" << endl
       << "Check: 'data/lecture-system-iter.dat' for generate solutions"
       << endl << endl;
}

vector <double> test_f2(double *x,vector <double> *y) 
{
  vector <double> result(*y);
  result.at(0) = -exp(-(*x)*y->at(1)) - sin(y->at(0));
  result.at(1) = y->at(0);

  return result;
}


vector <double> test_rf2(vector <double> *ya,
			 vector <double> *yb) 
{
  vector <double> result;
  result.push_back(ya->at(1));
  result.push_back(yb->at(1));
 return result;
}



/*
  second test: solve y" = 5.0*sinh(5*y);
  y(0) = 0; y(1) = 1;
  ( specificaly to test estimator class
    - problems tends to lead to singularities )
*/

void runtest3()
{
  cout << "============================================================" << endl
       << "Computing test 3 ... " <<endl;
  
  vector < double > f,x;

  FUNCTIONAL funcs(2);

  // set a linear fit as estimator
  x.push_back(0.0);
  x.push_back(1.0);
  f.push_back(0.0);
  f.push_back(1.0);
  f.push_back(1.0);

  funcs.estimator.set_parabol(f,x);
  funcs.estimator.max_relative_error = 0.2;
  
  // choose functions to evaluate
  funcs.r(test_rf3);
  funcs.f(test_f3);

  // get starting guess
  funcs.xk = x;
  funcs.sk_start = funcs.estimator.get_starting_guess(&x,2);

  funcs.print_iterations_to_file("data/node-generation-iter.dat");
  
  // do all the shooting
  funcs.solve_les();

  funcs.print_run_information();

  cout << "SUCCESS!" << endl
       << "Check: 'data/node-generation-iter.dat' for generate solutions"
       << endl << endl;
}


vector <double> test_f3(double *x,vector <double> *y) 
{
  vector <double> result(*y);
  result.at(0) = 5.0*sinh(5.0*y->at(1));
  result.at(1) = y->at(0);

  return result;
}


vector <double> test_rf3(vector <double> *ya,
			 vector <double> *yb) 
{
  vector <double> result;
  result.push_back(ya->at(1));
  result.push_back(1.0 - yb->at(1));
 return result;
}


/*
  fourth test: solve y" = -λy;
  y(0) = 0; y(1) = 1; ∫_0^1 y^2 dx = 1 
  (find 6 eigenvalue )
*/


void runtest4()
{
  cout << "============================================================" << endl
       << "Computing test 4 ... " <<endl;
  
  vector < double > f,x,s;

  FUNCTIONAL funcs(4);

  // initialize estimator
  f.push_back(1.0);
  f.push_back(1.0);

  x.push_back(0.0);
  x.push_back(1.0);

  funcs.estimator.set_sin(f,x);

  // choose functions to evaluate
  funcs.r(test_rf4);
  funcs.f(test_f4);
  
  funcs.xk = x;
  funcs.sk_start = funcs.estimator.get_starting_guess(&x,4);
  
  // find first eigenvalue
  funcs.print_iterations_to_file("data/solutions-in-infinite-well-iter.dat");

  funcs.solve_les();

  funcs.print_run_information();

  int n_eign=6;

  // search for 5 more solutions
  for (int i=2; i<=n_eign; i++)
    {
      // allocate estimator to nest sin-curve
      // and reset run
      funcs.grow_xk = 1;
      f.at(1) = i;
      funcs.estimator.set_sin(f,x);
      funcs.sk_start = funcs.estimator.get_starting_guess(&x,4);
      funcs.xk = x;

      funcs.estimator.set_sin(f,x);

      funcs.solve_les();

      funcs.print_run_information();
    }

    cout << "SUCCESS!" << endl
	 <<"Check: 'data/solutions-in-infinite-well-iter.dat' for generate solutions"
	 << endl << endl;
}


vector <double> test_f4(double *x,vector <double> *y) 
{
  vector <double> result(*y);
  result.at(0) = -y->at(2)*y->at(1);
  result.at(1) = y->at(0);
  result.at(2) = 0.0;
  result.at(3) = pow(y->at(1),2);

  return result;
}

vector <double> test_rf4(vector <double> *ya,
		   vector <double> *yb) 
{
  vector <double> result;
  result.push_back(ya->at(1));
  result.push_back(yb->at(1));
  result.push_back(ya->at(3));
  result.push_back(yb->at(3) - 1.0);
  return result;
}

/*
  fifth test: Schroedinger equation for an infinite deep well with two
              valleys. Solutions in constructed from case with one valley.
 */

void runtest5()
{
  cout << "============================================================" << endl
       << "Computing test 5 ... " <<endl;
  
  vector < double > f,x,s;

  FUNCTIONAL funcs(4);
  
  // set estimator parabola
  f.push_back(1.0);
  f.push_back(1.0);

  x.push_back(0.0);
  x.push_back(1.0);

  funcs.estimator.set_sin_cut(f,x);
  funcs.estimator.max_delta_x = 0.25;
  funcs.estimator.min_absolute_error = 0.1;
  funcs.estimator.max_relative_error = 0.15;
  
  // default system with different potential
  funcs.system->set_potential(test5_V0);
  
  // set limits and set intital guess from estimator
  x.at(1) = 4.0;
  funcs.xk = x;
  funcs.sk_start = funcs.estimator.get_starting_guess(&x,4);
  funcs.sk_start.at(2) = -1.0;

  // find solution to construct estimator
  funcs.solve_les();

  funcs.print_run_information();
  
  funcs.print_result_to_file("data/sym-antisym-base.dat",false,0.01);

  // setup second case (symmetric case)
  FUNCTIONAL funcs2(&funcs);

  // with different potential
  funcs2.system->set_potential(test5_V1);

  // and use first solution as estimator 
  vector <double> p1,p2;
  p1.push_back(0.0);  // x-shift
  p1.push_back(1.0);  // x-scale
  p1.push_back(1.0/sqrt(2.0));  // y-scale
  
  p2.push_back(4.0);  // x-shift
  p2.push_back(-1.0);  // x-scale
  p2.push_back(1.0/sqrt(2.0));  // y-scale

  // infect: multiple solution 2 times
  funcs2.estimator.set_two_funcitonal(&funcs, p1, &funcs, p2);
  funcs2.estimator.max_delta_x = 0.25;
  funcs2.estimator.min_absolute_error = 0.1;
  funcs2.estimator.max_relative_error = 0.2;

  // rearranging estimator masks
  funcs2.estimator.weight_factor.resize(4,0.0);
  funcs2.estimator.weight_factor.at(1) = 1.0;

  funcs2.xk = x;
  funcs2.sk_start = funcs.estimator.get_starting_guess(&x,4);

  funcs2.print_iterations_to_file("data/sym-antisym-iter.dat");

  // perform first search
  funcs2.solve_les();

  funcs2.print_run_information();

  funcs2.print_result_to_file("data/sym-antisym.dat",true,0.01);

  // redo the calculation as an anti-symmetric case
  p2.at(2) *= -1.0;
  funcs2.estimator.set_two_funcitonal(&funcs, p1, &funcs, p2);

  funcs2.xk = x;
  funcs2.sk_start = funcs.estimator.get_starting_guess(&x,4);
  funcs2.sk_start.at(3) = 0.0;
  funcs2.grow_xk = 1;
  funcs2.finished = false;

  // find second solution
  funcs2.solve_les();

  funcs2.print_run_information();

  funcs2.print_result_to_file("data/sym-antisym.dat",true,0.01);

  cout << "SUCCESS!" << endl
       <<"Check: 'data/sym-antisym{-base,-iter}.dat' for generate solutions"
       << endl << endl;
}

double test5_V0(double *x)
{
  if (*x < 0)
    return numeric_limits<double>::infinity();
  else if ( *x <= 1.0)
    return -2.0;
  else
    return 0.0;
}

double test5_V1(double *x)
{
  if (*x < 0)
    return numeric_limits<double>::infinity();
  else if ( *x <= 1.0)
    return -2.0;
  else if ( *x < 3.0 )
    return 0.0;
  else if ( *x <= 4.0 )
    return -2.0;
  else
    return numeric_limits<double>::infinity();
}


