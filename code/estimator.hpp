#ifndef _ESTIMATOR_H_
#define _ESTIMATOR_H_

/*
  to add further nodes
  -> introduction of an estimator class

  nested class in 'functional.hpp'
 */
class ESTIMATOR
{
private:
  vector <double> estimator_parameter;

  /*
    different function estimator
   */
  vector <double> eval_parabol(double *x, vector <double> *y);

  vector <double> eval_sin(double *x, vector <double> *y);

  vector <double> eval_sin_cut(double *x, vector <double> *y);  

  vector <double> eval_sin_cut2(double *x, vector <double> *y);  

  vector <double> eval_old_functional(double *x, vector <double> *y);  
  vector <double> eval_two_functional(double *x, vector <double> *y);  

  FUNCTIONAL *functionalptr1, *functionalptr2;
  
public:
  ESTIMATOR();
  ESTIMATOR(FUNCTIONAL *);
  /*
  virtual ~ESTIMATOR();
  */
  vector <double> (FUNCTIONAL::ESTIMATOR::*ptr2estimate)(double *, vector <double> *);
  
  double max_delta_x;
  double max_relative_error;
  double min_absolute_error;
  double multi;
  vector <double> weight_factor;
  vector <double> limit;
  
  bool check_estimator_derivation(vector <double>::iterator *cur_xk,
				  double *cur_x,
				  vector <double> *cur_y);

  vector <double> estimation_function(double *, vector <double> *);

  vector <double> get_starting_guess(vector <double> *,vector <double> *);
  vector <double> get_starting_guess(vector <double> *,int);

  void set_parabol(vector <double> &, vector <double> &);

  void set_sin(vector <double> &, vector <double> &);

  void set_sin_cut(vector <double> &, vector <double> &);

  void set_sin_cut2(vector <double> &, vector <double> &);

  void set_old_funcitonal(FUNCTIONAL *);
  void set_old_funcitonal(FUNCTIONAL *, vector <double> &);
  void set_two_funcitonal(FUNCTIONAL *, vector <double> &,
			   FUNCTIONAL *, vector <double> &);
}; 

#endif /* _ESTIMATOR_H_ */
