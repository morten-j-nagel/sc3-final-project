//====================================================================
//                
// Filename:      functional_eq_sys_handle.cpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Sat Jun  1 15:24:24 2013
// Modified at:   Wed Jun  5 14:37:36 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   conatins all the ugly functions to get the wished
//                equation system into the class.
// Update count:  0
//                
//====================================================================

#include "functional.hpp"

/*
  regulary functions evaluate in the code
*/

vector <double> FUNCTIONAL::f(double *x, vector <double> *y)
{
  return (this->*ptr2f)(x,y);
}

vector <double> FUNCTIONAL::r(vector <double> *ya,vector <double> *yb)
{
  return (this->*ptr2r)(ya,yb);
}

/*
  get system from regular functions (like defined in 'main.cpp')
 */
void FUNCTIONAL::f(vector <double> (*f_out)(double*, vector <double> *) )
{
  f_ext = f_out;
  ptr2f = &FUNCTIONAL::f_ext_eval;
  return;
}
void FUNCTIONAL::r(vector <double> (*r_out)(vector <double> *,
				vector <double> *) )
{
  r_ext = r_out;
  ptr2r = &FUNCTIONAL::r_ext_eval;
  return;
}
// evaluating normal functions
vector <double> FUNCTIONAL::f_ext_eval(double *x, vector <double> *y)
{
  return f_ext(x,y);
}
vector <double> FUNCTIONAL::r_ext_eval(vector <double> *ya, vector <double> *yb)
{
  return r_ext(ya,yb);
}

/*
  get system from class-function defined in the class 'DEFAULT_SYSTEM'
 */
void FUNCTIONAL::f(DEFAULT_SYSTEM *system_ext)
{
  f_sys = &DEFAULT_SYSTEM::f;
  
  if (system_ext != system && system != NULL)
    {
      delete system;
    }
  if (system_ext == NULL)
    {
      system_ext = new DEFAULT_SYSTEM;
      free_system = true;
    }
  
  system = system_ext;
  ptr2f = &FUNCTIONAL::f_sys_eval;
  return;
}
void FUNCTIONAL::r(DEFAULT_SYSTEM *system_ext)
{
  r_sys = &DEFAULT_SYSTEM::r;

  if (system_ext != system && system != NULL)
    {
      delete system;
    }
  if (system_ext == NULL)
    {
      system_ext = new DEFAULT_SYSTEM;
      free_system = true;
    }
  
  system = system_ext;
  ptr2r = &FUNCTIONAL::r_sys_eval;
  return;
}

// when called without options create a new default class
// (this functions are executed when FUNCTIONAl is initilize with a number)
void FUNCTIONAL::f()
{
  if (system == NULL)
    {
      system = new DEFAULT_SYSTEM;
      free_system = true;
    }
  f(system);
  return;
}
void FUNCTIONAL::r()
{
  if (system == NULL)
    {
      system = new DEFAULT_SYSTEM;
      free_system = true;
    }
  r(system);
  return;
}

// evaluate functions stored in default class
vector <double> FUNCTIONAL::f_sys_eval(double *x, vector <double> *y)
{
  return (system->*f_sys)(x,y);
}
vector <double> FUNCTIONAL::r_sys_eval(vector <double> *ya, vector <double> *yb)
{
  return (system->*r_sys)(ya,yb);
}
