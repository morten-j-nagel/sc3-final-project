#ifndef _SOLVE_LES_H_
#define _SOLVE_LES_H_

// used lapack functions to compute matrix inverse

extern "C" {
    // LU decomoposition of a general matrix
    void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);

    // generate inverse of a matrix given its LU decomposition
    void dgetri_(int* N, double* A, int* lda, int* IPIV,
		 double* WORK, int* lwork, int* INFO);
}

#include <cmath>
#include <vector>

#include "functional.hpp"
#include "main.hpp"
#include "vector_operators.hpp"
#include "ode_solve.hpp"

using namespace std;

#endif /* _SOLVE_LES_H_ */
