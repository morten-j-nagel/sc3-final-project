//====================================================================
//                
// Filename:      main.cpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Tue May 21 15:14:23 2013
// Modified at:   Wed Jun  5 15:02:13 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   main-file^^: manage console input + example system
// Update count:  0
//                
//====================================================================

#include "main.hpp"

/*
  main function
 */

int main (int argc, char **argv)
{

  // commandline-input handling:
  //   N = number - perform task number N from 'task.cpp'
  //   -t N - perform test number N from 'method_tests.cpp'
  //  'nothing' - execute make_run() function defined below

  for (int i=1; i<argc; i++)
    {
      // print help text
      if(strncmp(argv[i],"-h",2) == 0)
	{
	  usage_print_out(argv[0]);
	  return 0;
	}
      // select a test
      if(strncmp(argv[i],"-t",2) == 0)
	{
	  if (i+1 < argc)
	    {
	      select_run_test(argv[0], atoi(argv[i+1]));
	      return 0;
	    }
	  else
	    {
	      print_out_run_test_info(argv[0]);
	      return 0;
	    }
	}
    }

  // select a task
  if (argc == 2)
    {
      select_run_task(argv[0], atoi(argv[1]));
      return 0;
    }

  // else 
  usage_print_out(argv[0]);

  cout << endl << "Executing now example system defined in 'make_run()'"
       << " in 'main.cpp' (1D Schroedinger equation for V(x) = x^2) "
       << endl;

  make_run();
  return 0;
}

void usage_print_out(char *program_name)
{
  cout << "==========================================================================="
       << endl << endl
       << "Program written for 2013.1 Scientific Computing III University of Helsinki "
       << endl << endl
       << "==========================================================================="
       << endl << endl
       << "Tries via single of multiple shooting method to solve 1D ODE"
       << endl << "no input file - change code for different systems"
       << endl << "(or restrict your self to the default/testing systems)"
       << endl << endl
       << "Usage: " << endl << endl;
  print_out_run_task_info(program_name);
  print_out_run_test_info(program_name);
  cout << "No option = Execute now example function 'make_run()' from 'main.cpp' "
       << endl
       << "            (1D Schroedinger equation for V(x) = x^2)" << endl;
  return;
}

void make_run()
{
  /*
    as an example system:
      solve the schroedinger equation in atomic units for the potential

        V(x) = x^2

      The solution is obtain by using the multiple shooting method
      with an estimator function.  To solve the non-linear equation
      system between each shooting event the Newton iteration is used.
      During the first iteration intervals are choose from the
      estimatin function (set a new node, when derivation because too
      large - see below ).
      
   */

  /*
    the class FUNCTIONAL contains (see 'functional.hpp'):
      - equation system solver
      - integrator (predictor-corrector with variable step size)
      - a nested class providing the estimation function
      - function pointer to the given system
      - output handling functions
  */
  
  FUNCTIONAL funcs(4);
  /*
    funcs(4) means:
             -> set up system for 4 variables
             -> funcs.f = &DEFAULT_SYSTEM::f (see default_system.cpp)
	     -> funcs.r = &DEFAULT_SYSTEM::r
	     -> funcs.V = &DEFAULT_SYSTEM::inf_well_V
	     -> eps_tol = 1.0e-8 (convergence limit)
	     -> h_start = 1.0e-4 (start integrator step size)
	     -> grow_xk = 1 (place nodes during first newton iteration)
	     -> set_broyden_steps(0)
   */

  /*
    A decent guess for the estimator is crucial, as otherwise the
    solution tends to diverged when hitting singularities.  For the
    current potential V(x) = x^2 one would expect a solution with
    oscillation around x=0 and a declining slope at high absolute x
    values.  Therefore as estimation function is chosen a sin(x) in a
    interval, which is linearly extrapolated to zero outside of it:
    
    1 ++------+------+-**----+------*-------+---**-+-------+-----++
      +       +      +***    +     *+*      +   * *+       +      +
  0.8 ++              * *          * *          * *              ++
      |               *  *         *  *        *   *              |
  0.6 ++             *   *        *   *        *   *             ++
      |         ******    *       *   *        *   ******         |
  0.4 **********          *       *   *        *         **********
      |                   *      *     *       *                  |
  0.2 ++                  *      *     *      *                  ++
      |                   *      *     *      *                   |
    0 ++                   *     *     *      *                  ++
      |                    *     *     *     *                    |
 -0.2 ++                   *     *      *    *                   ++
      |                    *    *       *    *                    |
 -0.4 ++                   *    *       *    *                   ++
      |                    *    *        *   *                    |
 -0.6 ++                    *   *        *  *                    ++
      |                     *  *         *  *                     |
 -0.8 ++                    *  *         *  *                    ++
      +       +      +       **     +     ***      +       +      +
   -1 ++------+------+-------**-----+-----**+------+-------+-----++
     -4      -3     -2      -1      0       1      2       3      4

     Parameter here are chosen by try and error - the following one
     just "work" but may be suboptimal for convergence etc.  As
     estimator function can also previous computed FUNCTIONALs be
     taken (see runtest5() in 'method_tests.cpp' as an example).
  */
  vector <double> x,f,x_est;
  f.push_back(0.7); // normalizing
  f.push_back(1.0); // choose n - plot above with n=5

  // inner region
  x_est.push_back(-2.0);
  x_est.push_back(2.0);
  // starting guess for intervall where phi(x) might be none zero
  x.push_back(-3.0);
  x.push_back(3.0);

  // fill main class:
  //  - initialize the estimator function (see 'estimator.cpp'
  //    for other options)
  funcs.estimator.set_sin_cut2(f,x_est);

  // - set estimator options (hopefully self-explaining)
  funcs.estimator.max_relative_error = 0.2;
  funcs.estimator.min_absolute_error = 0.1;
  funcs.estimator.max_delta_x = 0.5;
  // estimator function gives only an estimate for phi and phi'
  // -> set other elements to zero 
  funcs.estimator.weight_factor.resize(4,0.0);
  funcs.estimator.weight_factor.at(1) = 1.0;

  // - Change potential to parabola (see 'default_system.cpp' or
  //   define own function and insert here a function pointer)
  funcs.system->set_potential(&DEFAULT_SYSTEM::parabola);

  // pipe all intermediate results into a file
  funcs.print_iterations_to_file("data/parabola-iter.dat");

  // set as intial nodes the begin and the end of the intergration interval
  funcs.xk = x;
  
  // do the shooting ;)
  cout << "Do shooting: " << endl;
  funcs.solve_les();

  funcs.print_run_information();
  
  // and slowly increase interval boundaries until energy eigenvalue
  // is converged: 0 - move negative x boundary
  //               1 - move positive x boundary
  // (done via a multiplication of 1.1 so x0 has to be neg. and x1 pos)
  funcs.find_infinite_value(&x,0);
  funcs.find_infinite_value(&x,1);

  // do some print out to std (number of nodes may be screwed by find
  // infinity procedure)
  funcs.print_run_information();

  // and write the final result to a file
  funcs.print_result_to_file("data/ground-state-parabola.dat",false);

  cout << "See 'data/ground-state-parabola.dat' for result!" << endl;
  return;
}
