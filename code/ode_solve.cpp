//====================================================================
//                
// Filename:      ode_solve.cpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Tue May 21 19:04:35 2013
// Modified at:   Thu Jun  6 15:04:26 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   predictor corrector ode-solver
// Update count:  0
//                
//====================================================================

#include "ode_solve.hpp"

/*

  compute Initial Value Problem for a certain function f with the
  boundary conditions formulated in 'r'.
  
 */

vector <double> FUNCTIONAL::compute_ivp(vector <double> *s,
					vector <double>::iterator *cur_xk,
					vector <double>::iterator *cur_sk,
					ofstream *datafile,
					bool bool_grow_xk)
{
  vector < double > all_x;
  vector < vector <double> > all_y;

  // solve ivp-problem with functions from 'ode_solve.cpp'
  predictor_corrector_solver(&all_x,&all_y,s, cur_xk, cur_sk,bool_grow_xk);

  vector <double> result;

  // report last y value back (to compute F_s or r(s_i,s_m) )
  result = all_y.back();

  // pipe result into ouput stream
  if (datafile != NULL)
    {
      for (unsigned int i = 0; i < all_x.size(); ++i)
	{
	  *datafile << all_x.at(i);
	  for (vector <double>::iterator yi = all_y.at(i).begin();
	       yi != all_y.at(i).end(); yi++)
	    *datafile << " " << *yi;
	  *datafile << endl;
	}
	  
      *datafile << endl;
      datafile->flush();
    }

  // compute in case of single shooting residual
  // (don't do it when used as estimator)
  if (! multi && ! finished )
    result = r(s,&result);
  
  return result;
}


/*

  predictor corrector algorithm with timestep controller
  ( be aware that a lot operators (+,-,/,...) are overloaded :) )
  
 */

void FUNCTIONAL::predictor_corrector_solver(vector <double> *x,
					    vector <vector <double> > *eta,
					    vector <double> *eta0,
					    vector <double>::iterator *cur_xk,
					    vector <double>::iterator *cur_sk,
					    bool bool_grow_xk)
{
  // just to be sure eta is clear ;)
  x->resize(0);
  eta->resize(0);

  // save start values
  x->push_back(*(*cur_xk));
  eta->push_back(*eta0);

  // variables
  double h_now=h_start, h_next=h_start, h_old;
  
  if (detect_singularities)
    h_old = h_start;
  else
    h_old = (h_start*h_start<1e-12?1e-12:h_next*h_start);
    
  double s_p,e_p;

  // initial start step:
  runge_kutta_step(x,eta,h_old);

  // allocate storage arrays (none-const due too lazy extending operators)
  vector <double> previous_predictor_f(*eta0),
    predictor_f(*eta0), corrector_f(*eta0);
  vector <double> predictor_eta(*eta0),
    corrector_eta1(*eta0), corrector_eta2(*eta0);

  previous_predictor_f = f(&(*(*cur_xk)), eta0);

  do 
    {
      // don't overshoot / hit exact end-point
      if ( x->back() + h_now > *(*cur_xk+1))
	h_now = *(*cur_xk+1) - x->back();

      // predictor evaluation
      predictor_f = f(&(x->back()), &(eta->back()));

      s_p = h_now/h_old;

      predictor_eta = eta->back()
	+ ( predictor_f * (1.0 + 0.5*s_p)
	    - previous_predictor_f*0.5*s_p)*h_now;

      // evaluation of the 2 corrector
      corrector_f = f(&(x->back()), &predictor_eta);
	
      corrector_eta1 = eta->back()
	+ ( corrector_f + predictor_f ) * 0.5*h_now;

      corrector_eta2 = eta->back()
	+ ( corrector_f*(3.0 + 2.0*s_p)
	    + predictor_f*(3.0 + 4.0*s_p + pow(s_p,2))
	    - previous_predictor_f*pow(s_p,2)
	    )*h_now / (6.0*(1.0+s_p));

      // compute difference between corrector results
      e_p = l2_norm(corrector_eta1-corrector_eta2);

      // compute next time-step
      h_next = 0.9 * h_now * pow(eps_tol/e_p,1.0/4.0);

      // prevent too large changes in timestep
      if (h_next < 0.5*h_now)
	h_next = 0.5*h_now;
      else if (h_next > 1.5*h_now)
	h_next = 1.5*h_now;

      // catch too small time-steps
      if (h_next < eps_tol)
	  h_next = eps_tol;


      // accept step if either difference between correctors is small enough
      // or the step-length is too small anyway...
      if ( (e_p < eps_tol) || (h_now == eps_tol) )
	{
	  x->push_back(x->back() + h_now);
	  
	  h_old = h_now;
	  h_now = h_next;
	  eta->push_back(corrector_eta2);
	  previous_predictor_f = predictor_f;
	  /*
	    stop integration and insert a new root point when:
	    - estimator demands it
	    - or h->0 = hitting a singularity 
	   */
	  
	  if ( bool_grow_xk
	      && ( (estimator.check_estimator_derivation(cur_xk,
							 &(x->back()),
							 &(eta->back())) )
		   || ((h_old == eps_tol) && detect_singularities )) )
	    {
	      int k = *cur_xk - xk.begin();

	      // insert next point after current point
	      xk.insert(*cur_xk+1, x->back());
	      vector <double> estimate_eta = estimator.estimation_function(&x->back(),
									   &eta->front());
	      
	      s.insert(*cur_sk+eps_s.size(), estimate_eta.begin(),
		       estimate_eta.end());

	      // update iterators
	      *cur_xk = xk.begin() + k;
	      *cur_sk = s.begin() + k*eps_s.size();

	      // and increase delta_F_s-size
	      // (all other matrices, which are to addjust, change implicitly)
	      grow_matrix(&delta_F_s, eps_s.size());
	    }
	}
      else
	h_now = h_next;
      
      // repeat process until x end-point is reached
    } while (x->back() < *(*cur_xk+1));

  return;
}

/*

  Do a 4th order Runge kutta step (at the start for example...)
  
 */

void FUNCTIONAL::runge_kutta_step(vector <double> *x,
				  vector <vector <double> > *eta,
				  double h)
{
  
  double x_temp;
  vector <double> eta_temp;
  
  // Runge-Kutta constants
  vector <double> k1(eta->back()),k2(eta->back()),
    k3(eta->back()),k4(eta->back());
  
  x_temp = x->back();
  eta_temp = eta->back();
  k1 = f(&x_temp,&eta_temp);
  
  x_temp += 0.5*h;
  eta_temp = eta->back() + k1*0.5*h;
  k2 = f(&x_temp,&eta_temp);
 
  eta_temp = eta->back() + k2*0.5*h;
  k3 = f(&x_temp,&eta_temp);
  
  x_temp += 0.5*h;
  eta_temp = eta->back() + k3*h;
  k4 = f(&x_temp,&eta_temp);
  
  // save step
  x->push_back(x_temp);
  eta->push_back(eta->back() + (k1 + (k2 + k3)*2.0 + k4)*h/6.0 );
}

