//====================================================================
//                
// Filename:      estimator.cpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Fri May 31 10:38:31 2013
// Modified at:   Wed Jun  5 14:55:34 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   define estimation function providing starting
//                guesses using the multiple shooting method
// Update count:  0
//                
//====================================================================

#include "functional.hpp"

/*
  ESTIMATOR functions

  There are 5 different estimators present:

  - a parabola between two points (computed with the derivation at the
    first point.
  - 'sin' between two points with n oscillation
  - 'sin_cut' = sin cut at the right side and linear extrapolated
  - 'sin_cut2' = sin cut and extrapolated at both sides
  - 'old_functional' = use a function with a solution
  - 'two_functional' = combine 2 functionas containing a solution
  
 */

// initialize comparison variables
FUNCTIONAL::ESTIMATOR::ESTIMATOR()
{
  max_delta_x = 1.0;
  max_relative_error = 0.1;
  min_absolute_error = 1e-4;
  
  ptr2estimate = NULL;
  multi = true;
}

// initialize with a functional class -> made previous run the estimator
// for the current one
FUNCTIONAL::ESTIMATOR::ESTIMATOR(FUNCTIONAL *old_func)
{
  max_delta_x = 1.0;
  max_relative_error = 0.1;
  min_absolute_error = 1e-4;
  
  multi = true;
  set_old_funcitonal(old_func);
}

/*
  fit a parabol between f(x0) and f(x1) providing f'(x1)
 */

vector <double> FUNCTIONAL::ESTIMATOR::eval_parabol(double *x, vector <double> *y)
{
  vector <double> result(*y);

  // estimator_parameter = {a,b,c} in ax^2 + bx + c
  
  result.at(0) = (*x*2.0*estimator_parameter.at(0) + estimator_parameter.at(1)); // f'(x)
  result.at(1) = (*x*(*x*estimator_parameter.at(0) + estimator_parameter.at(1))
		   + estimator_parameter.at(2) ); // f(x)

  return result;
}

/*
  compute parabol parameters from a starting point with a start derivative
  to a end point
*/

void FUNCTIONAL::ESTIMATOR::set_parabol(vector <double> &f, vector <double> &x)
{
  if (f.size() != 3 && x.size() != 2)
    return;

  estimator_parameter.resize(3,0.0);

  // a
  estimator_parameter.at(0) = (f.at(2) - f.at(0) - f.at(1)*(x.at(1) - x.at(0)))
    /pow(x.at(1) - x.at(0),2);

  // c
  estimator_parameter.at(2) =  f.at(0) - x.at(0)*(f.at(1)
					     - estimator_parameter.at(0)*x.at(0));
  // b
  estimator_parameter.at(1) = f.at(1) - 2.0*estimator_parameter.at(0)*x.at(0);

  ptr2estimate = &FUNCTIONAL::ESTIMATOR::eval_parabol;

  limit = x;
  
  return;
}

/*
  Fit a sin function between x0 and x1 with int sin(ax) dx = 1 in this region
 */

vector <double> FUNCTIONAL::ESTIMATOR::eval_sin(double *x, vector <double> *y)
{
  vector <double> result(*y);
  double shifted_x = *x - estimator_parameter.at(3);

  // estimator_parameter = {a,b,c} in f = b*sin(ax); f' = c*cos(ax)
  if ( *x < limit.at(0) )
    {
      result.at(0) = 0.0;
      result.at(1) = 0.0;
    }
  else if ( *x > limit.at(1) )
      {
	result.at(0) = 0.0;
	result.at(1) = 0.0;	
      }
  else
    {
      result.at(0) = estimator_parameter.at(2)
	*cos(estimator_parameter.at(0)*shifted_x); // f'(x)
      result.at(1) = estimator_parameter.at(1)
	*sin(estimator_parameter.at(0)*shifted_x);
    }
  
  return result;
}

void FUNCTIONAL::ESTIMATOR::set_sin(vector <double> &f, vector <double> &x)
{
  if (f.size() != 2 && x.size() != 2)
    return;

  // f = {V, n}
  
  estimator_parameter.resize(4,0.0);

  estimator_parameter.at(0) = f.at(1)*atan(1.0)*4.0/(x.at(1) - x.at(0));
  estimator_parameter.at(1) = f.at(0)*sqrt(2.0);
  estimator_parameter.at(2) = estimator_parameter.at(1)*estimator_parameter.at(0);
  estimator_parameter.at(3) = x.at(0);

  ptr2estimate = &FUNCTIONAL::ESTIMATOR::eval_sin;

  limit = x;

  return;
}

/*
  simalar to sin but with f(x1) != 0 -> linear cut-off to x1 + 2.0*(x1-x0)
  and no normalization - aka. choose a appropriate scaling factor
 */

vector <double> FUNCTIONAL::ESTIMATOR::eval_sin_cut(double *x, vector <double> *y)
{
  vector <double> result(*y);
  double shifted_x = *x - estimator_parameter.at(7);

  // estimator_parameter = {a,b,c} in ax^2 + bx + c
  if ( *x < limit.at(0) )
    {
      result.at(0) = 0.0;
      result.at(1) = 0.0;
    }
  else if ( *x <= estimator_parameter.at(3))
    {
      result.at(0) = estimator_parameter.at(2)
	*cos(estimator_parameter.at(0)*shifted_x); // f'(x)
      result.at(1) = estimator_parameter.at(1)
	*sin(estimator_parameter.at(0)*shifted_x);
    }
  else if ( *x <= estimator_parameter.at(6))
    {
      result.at(0) = estimator_parameter.at(4);
      result.at(1) = estimator_parameter.at(4)*
	(shifted_x-estimator_parameter.at(3)) + estimator_parameter.at(5);
    }
  else
    {
      result.at(0) = 0.0;
      result.at(1) = 0.0;
    }
      
  return result;
}

void FUNCTIONAL::ESTIMATOR::set_sin_cut(vector <double> &f, vector <double> &x)
{
  if (f.size() != 2 && x.size() != 2)
    return;

  // f = {V, n}
  
  estimator_parameter.resize(8,0.0);

  estimator_parameter.at(0) = atan(1.0)*4.0 / (x.at(1) - x.at(0))
    * (6.0*f.at(1) - 1.0)/6.0;
  estimator_parameter.at(1) = f.at(0)*sqrt(2.0);
  estimator_parameter.at(2) = estimator_parameter.at(1)*estimator_parameter.at(0);
  estimator_parameter.at(3) = x.at(1);

  double ym = estimator_parameter.at(1)*sin(estimator_parameter.at(0)
					    *(x.at(1) - x.at(0)));

  //cout << ym << " " << estimator_parameter.at(1)*0.5 << endl;
  
  estimator_parameter.at(6) = (3.0*x.at(1) - 2.0*x.at(0));
  estimator_parameter.at(4) = -ym/(estimator_parameter.at(6) - x.at(1));
  estimator_parameter.at(5) = ym;
  estimator_parameter.at(7) = x.at(0);

  ptr2estimate = &FUNCTIONAL::ESTIMATOR::eval_sin_cut;

  limit = x;
  limit.at(1) = (3.0*x.at(1) - 2.0*x.at(0));
  
  return;
}

/*
  similar to sin_cut but also cut and linear extrapolated at x0
  (WARNING: may not completely debugged (no smooth transistion) )
 */

vector <double> FUNCTIONAL::ESTIMATOR::eval_sin_cut2(double *x, vector <double> *y)
{
  vector <double> result(*y);
  double shifted_x = *x + estimator_parameter.at(5);

  
  // estimator_parameter = {a,b,c} in ax^2 + bx + c
  if (*x <= estimator_parameter.at(7))
    {
      result.at(0) = 0.0;
      result.at(1) = 0.0;
    }
  else  if (*x < estimator_parameter.at(4) )
    {
      result.at(0) = estimator_parameter.at(9);
      result.at(1) = estimator_parameter.at(9)*(shifted_x-estimator_parameter.at(7)
						- 2.0);
    }
  else if ( *x <= estimator_parameter.at(5))
    {
      result.at(0) = estimator_parameter.at(3)
	*cos(estimator_parameter.at(1)*shifted_x + estimator_parameter.at(0)); // f'(x)
      result.at(1) = estimator_parameter.at(2)
	*sin(estimator_parameter.at(1)*shifted_x + estimator_parameter.at(0));
    }
  else if ( *x <= estimator_parameter.at(6))
    {
      result.at(0) = estimator_parameter.at(8);
      result.at(1) = estimator_parameter.at(8)*(shifted_x-estimator_parameter.at(6)
						- 2.0);
    }
  else
    {
      result.at(0) = 0.0;
      result.at(1) = 0.0;
    }
  
  return result;
}

void FUNCTIONAL::ESTIMATOR::set_sin_cut2(vector <double> &f, vector <double> &x)
{
  if (f.size() != 2 && x.size() != 2)
    return;

  // f = {V, n}
  
  estimator_parameter.resize(10,0.0);

  estimator_parameter.at(0) = atan(1.0)*4.0/6.0;
  estimator_parameter.at(1) = estimator_parameter.at(0) / (x.at(1) - x.at(0))
    * (6.0*f.at(1) - 2.0);
  estimator_parameter.at(2) = f.at(0)*sqrt(2.0);
  estimator_parameter.at(3) = estimator_parameter.at(1)*estimator_parameter.at(0);
  estimator_parameter.at(4) = x.at(0);
  estimator_parameter.at(5) = x.at(1);

  double ym1 = estimator_parameter.at(2)
    *sin(estimator_parameter.at(1)*(x.at(1) - x.at(0)) + estimator_parameter.at(0));
  double ym2 = estimator_parameter.at(2)
    *sin(estimator_parameter.at(0));

  //cout << ym << " " << estimator_parameter.at(1)*0.5 << endl;
  
  estimator_parameter.at(6) = (3.0*x.at(1) - 2.0*x.at(0));
  estimator_parameter.at(7) = (3.0*x.at(0) - 2.0*x.at(1));
  estimator_parameter.at(8) = -ym1/(estimator_parameter.at(6) - x.at(1));
  estimator_parameter.at(9) = -ym2/(estimator_parameter.at(7) - x.at(0));
    
  ptr2estimate = &FUNCTIONAL::ESTIMATOR::eval_sin_cut2;
  
  limit = x;
  limit.at(0) = estimator_parameter.at(7);
  limit.at(1) = estimator_parameter.at(6);
  
  return;
}

/*
  functionals have a function 'get_solution_value' which calculates
  for the provided x-value the y-values of the found solution.
  Pointer to old functional is saved to be call from new functional.

  Thereby: estimator_parameter
   0 - shift in x-direction
   1 - scale of x value
   2 - scale of y values (all value are scaled!)

   if called with out parameter array, assume [0.0, 1.0, 1.0]

   Same goes when using two functionals
   (BEAWARE: two functionals have no limit handling!)
   
 */

//functions called to get an estimator guess
vector <double> FUNCTIONAL::ESTIMATOR::eval_old_functional(double *x,
							   vector <double> *y)
{
  double shifted_x;
  vector <double> result;
  
  if (*x < limit.at(0) )
    shifted_x = limit.at(0);
  else if (*x > limit.at(1) )
    shifted_x = limit.at(1);
  else
    shifted_x  = (*x - estimator_parameter.at(0))*estimator_parameter.at(1);

  result = functionalptr1->get_solution_value(&shifted_x,y)*estimator_parameter.at(2);

  // don't reuse old energy value
  result.at(2) = y->at(2);
  
  return result;
}
vector <double> FUNCTIONAL::ESTIMATOR::eval_two_functional(double *x,
							   vector <double> *y)
{
  double shifted_x1;
  vector <double> result;
  
  shifted_x1  = (*x - estimator_parameter.at(0))*estimator_parameter.at(1);

  double shifted_x2 = (*x - estimator_parameter.at(3))*estimator_parameter.at(4);
  result = ( functionalptr1->get_solution_value(&shifted_x1,y)
	     *estimator_parameter.at(2)
	     + functionalptr2->get_solution_value(&shifted_x2,y)
	     *estimator_parameter.at(5) );

  // don't reuse old energy value
  result.at(2) = y->at(2);
  
  return result;

}

// functions to handle pointer and scaling+shift-factors
//  - default case without parameters
void FUNCTIONAL::ESTIMATOR::set_old_funcitonal(FUNCTIONAL *old_func)
{
  vector <double> parameter;
  parameter.push_back(0.0);
  parameter.push_back(1.0);
  parameter.push_back(1.0);
  set_old_funcitonal(old_func,parameter);
  return;
}
// - with parameters
void FUNCTIONAL::ESTIMATOR::set_old_funcitonal(FUNCTIONAL *old_func,
					       vector <double> &parameter)
{
  if (parameter.size() != 3)
      return;
  
  functionalptr1 = old_func;
  estimator_parameter = parameter;

  ptr2estimate = &FUNCTIONAL::ESTIMATOR::eval_old_functional;

  limit.resize(2,0.0);
  limit.at(0) = (old_func->xk.front()
		 - estimator_parameter.at(0))*estimator_parameter.at(1);
  limit.at(1) = (old_func->xk.back()
		 - estimator_parameter.at(0))*estimator_parameter.at(1);

  return;
}

/*
  using two functionals, which have to contain a solution.

  Otherwise, scaling and shifting is the same as with one functional
  (see above)
 */

void FUNCTIONAL::ESTIMATOR::set_two_funcitonal(FUNCTIONAL *old_func1,
					       vector <double> &parameter1,
					       FUNCTIONAL *old_func2,
					       vector <double> &parameter2)
{
  if (parameter1.size() != 3 && parameter2.size() != 3)
      return;
  
  functionalptr1 = old_func1;
  estimator_parameter = parameter1;
  functionalptr2 = old_func2;
  estimator_parameter.insert(estimator_parameter.end(),parameter2.begin(),
			     parameter2.end());
  
  ptr2estimate = &FUNCTIONAL::ESTIMATOR::eval_two_functional;

  limit.resize(2,0.0);
  limit.at(0) = (old_func2->xk.front()
		 - estimator_parameter.at(0))*estimator_parameter.at(1);
  limit.at(1) = (old_func2->xk.back()
		 - estimator_parameter.at(0))*estimator_parameter.at(1);

  return;
}

bool FUNCTIONAL::ESTIMATOR::check_estimator_derivation(vector <double>::iterator *cur_xk,
						       double *cur_x,
						       vector <double> *cur_y)
{
  // get eta from the estimator
  // and compare it with the eta of the current calculation
  // ( | -  multiply elements piece-wise)
  vector <double> eta = estimation_function(cur_x, cur_y) | weight_factor;
  double eta2 = l2_norm(eta);
  
  // if maximum interval length is reach 
  if ( abs(*(*cur_xk) - *cur_x) > max_delta_x)
    return true;
  
  // or relaitve error is too large
  else if ( ( ( l2_norm( (*cur_y | weight_factor) - eta )
		/ eta2 ) > max_relative_error )
	    && ( eta2 > min_absolute_error) )
    return true;
  
  return false;
}

// wrapper function for function pointer to specific estimator
vector <double> FUNCTIONAL::ESTIMATOR::estimation_function(double *x, vector <double> *y)
{
  return (this->*ptr2estimate)(x,y);
}

/*
  use the estimate to get some starting guesses
 */

vector <double> FUNCTIONAL::ESTIMATOR::get_starting_guess(vector<double> *xk,
					      vector <double> *s_start)
{
  vector <double> result;
  vector <double> sk(*s_start);
  
  for(vector <double>::iterator xi=xk->begin(); xi != xk->end(); xi++)
    {
      sk = this->estimation_function(&(*xi),s_start);
      result.insert(result.end(), sk.begin(), sk.end());
      // singel shooting method return only starting value guess
      if (!multi)
	break;
    }

  return result;
}

/*
  if no simple s in present (which may contain parts not convered by
  the estimator - like eigenvalue guesses ) start with an empty one of
  the right size
*/
vector <double> FUNCTIONAL::ESTIMATOR::get_starting_guess(vector<double> *xk, int n)
{
  vector <double> s_start;
  s_start.resize(n,0.0);
  return get_starting_guess(xk,&s_start);
}
