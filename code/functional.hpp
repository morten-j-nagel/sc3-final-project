#ifndef _FUNCTIONAL_H_
#define _FUNCTIONAL_H_

#include <vector>
#include <cmath>
#include <iostream>
#include <string>
#include <fstream>

#include "default_system.hpp"
#include "vector_operators.hpp"

using namespace std;

/*
  dummy class to store function pointers etc.
 */

class FUNCTIONAL
{
private:

  // add nested estimator class
#include "estimator.hpp"

  // internal variables
  int broyden_steps;
  ofstream *iteration_datafile;
  ofstream *results_datafile;

  /*
    funcstion need to perform the eqaution system solving
    (more precisely functions building the ES)
   */
  void single_F_s(bool bool_delta_F_s);
  
  void multi_F_s(bool bool_delta_F_s);

  void (FUNCTIONAL::*compute_F_s)(bool bool_delta_F_s);

  void compute_delta_F_s(vector <double> *F_s,
			 vector <double> *delta_F_s,
			 vector <double> *s,
			 vector <double>::iterator *xk);

  void compute_delta_F_s_A_B(vector <double> *A,
			     vector <double> *B);

  vector <double> get_inverse(vector <double> *delta_F_s);

  
  // eqautions system storage variables
  vector <double> s;
  vector <double> F_s;
  vector <double> delta_F_s;

  /* 
     ode_solver functions
  */
  vector <double> compute_ivp(vector <double> *s,
			      vector <double>::iterator *xk,
			      vector <double>::iterator *sk,
			      ofstream *,
			      bool grow_xk);

  void predictor_corrector_solver(vector <double> *,
				  vector <vector <double> > *,
				  vector <double> *,
				  vector <double>::iterator *xk,
				  vector <double>::iterator *sk,
				  bool grow_xk);

  void runge_kutta_step(vector <double> *,
			vector <vector <double> > *,
			double );

  // pointer foo

  vector <double> (FUNCTIONAL::*ptr2f)(double*, vector <double> *);
  vector <double> (FUNCTIONAL::*ptr2r)(vector <double> *,
				       vector <double> *);

  // normal functions (in main etc ...)
  vector <double> f_ext_eval(double*, vector <double> *);
  vector <double> r_ext_eval(vector <double> *,
			     vector <double> *);
    
  vector <double> (*f_ext)(double*, vector <double> *);
  vector <double> (*r_ext)(vector <double> *,
			   vector <double> *);

  // functions from 'default_system' class
  vector <double> f_sys_eval(double*, vector <double> *);
  vector <double> r_sys_eval(vector <double> *,
			     vector <double> *);
    
  vector <double> (DEFAULT_SYSTEM::*f_sys)(double*, vector <double> *);
  vector <double> (DEFAULT_SYSTEM::*r_sys)(vector <double> *,
					   vector <double> *);
  
public:
  FUNCTIONAL();
  FUNCTIONAL (int);
  FUNCTIONAL (FUNCTIONAL *);
  virtual ~FUNCTIONAL();

  // switch variables
  bool detect_singularities; // MSM: set node when h->0
  int grow_xk; // number of newton iteration steps where nodes are added

  vector <double> f(double*, vector <double> *);
  vector <double> r(vector <double> *,vector <double> *);

  void f(vector <double> (*f)(double*, vector <double> *));
  void r(vector <double> (*r)(vector <double> *,
			      vector <double> *) );

  void f(DEFAULT_SYSTEM * );
  void r(DEFAULT_SYSTEM * );

  void f();
  void r();
  
  vector <double> eps_s;
  vector <double> xk; // root points
  vector <double> sk_start,sk_end;
  double eps_tol;
  double h_start;

  // controll class for new nodes
  ESTIMATOR estimator;
  DEFAULT_SYSTEM *system;
  
  int n_iter; // counter for iterations steps

  // perform whole computation
  vector <double> solve_les();

  // switch functions
  void set_multiple_shooting();
  void set_single_shooting();
  void set_broyden_steps(int steps);

  
  // file-print out functions

  void print_result_to_file(string,bool cont=false, double dx=0.05);
  void print_result_to_file();
  void close_print_result_to_file();
  void print_iterations_to_file(string);
  void print_iterations_to_file();

  void print_run_information();
  void print_class_content();

  // report found t=solution back
  vector <double> get_solution_value(double *x, vector <double> *y);
  vector <double>::iterator get_solution_iterator;

  // find infinite values by slowly increasing x limits
  void find_infinite_value(vector <double> *x, int l=1);


  // switches
  bool free_system;
  bool multi; // single or multiple-shooting
  bool finished;
  
};


#endif /* _FUNCTIONAL_H_ */




