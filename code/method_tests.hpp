#ifndef _METHOD_TESTS_H_
#define _METHOD_TESTS_H_

#include <vector>
#include <cmath>

#include "functional.hpp"
#include "vector_operators.hpp"
#include "default_system.hpp"

using namespace std;

void select_run_test(char *, int );
void print_out_run_test_info(char *);

// first runtest
void runtest1();
vector <double> test_f1(double *,vector <double> *);
vector <double> test_rf1(vector <double> *,vector <double> *);

// second runtest
void runtest2();
vector <double> test_f2(double *,vector <double> *);
vector <double> test_rf2(vector <double> *,vector <double> *);

// third runtest
void runtest3();
vector <double> test_f3(double *,vector <double> *);
vector <double> test_rf3(vector <double> *,vector <double> *);

// fourth runtest
void runtest4();
vector <double> test_f4(double *,vector <double> *);
vector <double> test_rf4(vector <double> *,vector <double> *);

// fifth runtest
void runtest5();
double test5_V0(double *);
double test5_V1(double *);

#endif /* _METHOD_TESTS_H_ */
