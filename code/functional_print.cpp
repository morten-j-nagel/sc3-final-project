//====================================================================
//                
// Filename:      functional_print.cpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Sat Jun  1 15:19:37 2013
// Modified at:   Fri Jun  7 22:18:54 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   contains std and file output functions
// Update count:  0
//                
//====================================================================

#include "functional.hpp"

// file operations:
void FUNCTIONAL::print_result_to_file(string filename="data.dat",
				      bool cont, double dx)
{
  if(results_datafile == NULL)
    {
      results_datafile = new ofstream;
      results_datafile->open(filename.c_str());
    }

  double x = xk.front(), x_end = xk.back();
  vector <double> dummy_y, y;
  dummy_y.resize(4,0.0);

  // using class-own function 'get_solution_value' to print results to
  // data-file
  do
    {
      *results_datafile << x;
      if (x>x_end)
	x = x_end;
      y = get_solution_value(&x, &dummy_y);
      for (vector <double>::iterator yi = y.begin();
	   yi != y.end(); yi++)
	{
	  *results_datafile << " " << *yi;
	}
      
      *results_datafile << endl;
      if (x == x_end)
	break;
      
      x += dx;
      
    } while (true);

  *results_datafile << endl << endl;
  results_datafile->flush();
  
  if (!cont)
    {
      results_datafile->close();
      delete results_datafile;
      results_datafile = NULL;
    }

  return;
}
// overloaded it for lazy usage :)
void FUNCTIONAL::print_result_to_file()
{
  print_result_to_file("data.dat",false);
  return;
}

void FUNCTIONAL::close_print_result_to_file()
{
  results_datafile->close();
  delete results_datafile;
  results_datafile = NULL;
  
  return;
}

// pipe every calculation done by the integrator to a file
void FUNCTIONAL::print_iterations_to_file(string filename="data-iter.dat")
{
  if (iteration_datafile != NULL)
    {
      iteration_datafile->close();
      delete iteration_datafile;
    }
  iteration_datafile = new ofstream;
  iteration_datafile->open(filename.c_str());
  return;
}
// overloaded it for lazy usage :)
void FUNCTIONAL::print_iterations_to_file()
{
  print_iterations_to_file("data-iter.dat");
  return;
}

// std operations:

void FUNCTIONAL::print_run_information()
{
  vector <string> modes;
  modes.push_back("single shooting");
  modes.push_back("multiple shooting");

  cout << "Converged in " << n_iter << " iterations"
       << " ( ||F_s|| = " << max_norm(F_s) << " )" << endl
       << " with " << xk.size() << " nodes" 
       << " using " << modes.at(multi) << " mode" << endl;
  if (broyden_steps > 1)
    cout  << " and " << broyden_steps - 1
	  << " Broyden steps per Newton step" << endl;
  cout << " Conv. limit: " << eps_tol
       << ", start h: " << h_start << endl
       << " Solution starts with: [ ";
  for (unsigned int i=0; i<eps_s.size(); i++)
    cout << s.at(i) << " ";
  cout << " ]" << endl
       << " Solution interval: [ " << xk.front()
       << " " << xk.back() << " ]" << endl;
  
  cout << endl;
  return;
}

/*
  Dump class content to screen
  (may/will not contain all variables)
 */

void FUNCTIONAL::print_class_content()
{
  cout << "Control:" << endl
       << "  Multi: " << multi << endl
       << "  Singularities: " << detect_singularities << endl
       << "  grow_xk: " << grow_xk << endl 
       << "  Broyden_steps: " << broyden_steps << endl
       << "  Finished: " << finished << endl
       << "  h_start: " << h_start << endl
       << "  eps_tol: " << eps_tol << endl
       << "  Estimator: " << endl
       << "    maximum intervall size: " << estimator.max_delta_x << endl
       << "    maximum reslative error: " << estimator.max_relative_error << endl
       << "    minimum absolute error: " << estimator.min_absolute_error << endl
       << endl;
  
  cout << "Results:" << endl
       << "  iterations: " << n_iter << endl
       << "  eps_s: ";
  write_vector(&eps_s);
  cout << endl << "  sk_start: ";
  write_vector(&sk_start);
  cout << endl << "  xk: ";
  write_vector(&xk);
  cout << endl << "  s: ";
  write_vector(&s);
  cout << endl << "  sk_end: ";
  write_vector(&sk_end);
  cout << endl << endl;
  
  return;
}
