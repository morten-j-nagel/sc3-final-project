//====================================================================
//                
// Filename:      default_system.cpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Fri May 31 10:52:31 2013
// Modified at:   Mon Jun  3 20:57:32 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   class containing the default equation system
// Update count:  0
//                
//====================================================================

#include "default_system.hpp"

/*
  infinite deep potential well between x0=0 and x1=1
 */

DEFAULT_SYSTEM::DEFAULT_SYSTEM()
{
  V = &DEFAULT_SYSTEM::inf_well_V;
}

/*
  Schroedinger Equation in 1D and atomic units (h=1, m=1)
  with NO free boundary at the right side
 */

vector <double> DEFAULT_SYSTEM::f(double *x,vector <double> *y) 
{
  vector <double> result(*y);
  double result_V = (this->*V)(x);
  if (!isinf(result_V))
    {
      result.at(0) = 2.0*(result_V - y->at(2))*y->at(1);
      result.at(1) = y->at(0);
      result.at(2) = 0.0;	// energy eigenvalue
      result.at(3) = pow(y->at(1),2);
    }
  else
    {
      result.at(0) = 0.0;
      result.at(1) = 0.0;
      result.at(3) = pow(y->at(1),2);
    }
      
  return result;
}

vector <double> DEFAULT_SYSTEM::r(vector <double> *ya,
				  vector <double> *yb) 
{
  vector <double> result;
  result.push_back(ya->at(1)); // phi(x_start) = 0
  result.push_back(yb->at(1)); // phi(x_end) = 0
  result.push_back(ya->at(3)); // 
  result.push_back(yb->at(3) - 1.0); // ∫ phi^2 dx = 1 (normalization)
  return result;
}

/*
  Schroedinger Equation in 1D and atomic units (h=1, m=1)
  with a free boundary at the right side.
  BUT: the given boundary conditions in 'r2' do not lead to convergence :(
       (if the normalization derivative is zero at both ends)
 */

vector <double> DEFAULT_SYSTEM::f2(double *x,vector <double> *y) 
{
  double a = y->at(5);
  vector <double> result(*y);
  double scal_x = *x*a;
  double result_V = (this->*V)(&scal_x);
  if (!isinf(result_V))
    {
      result.at(0) = a*2.0*(result_V - y->at(2))*y->at(1);
      result.at(1) = a*y->at(0);
      result.at(2) = 0.0;	// energy eigenvalue
      result.at(3) = a*pow(y->at(1),2);
      result.at(4) = a*y->at(1)*y->at(0)*2.0;
      result.at(5) = 0.0;	// free boundary
    }
  else
    {
      result.at(0) = 0.0;
      result.at(1) = 0.0;
      result.at(2) = 0.0;
      result.at(3) = a*pow(y->at(1),2);
      result.at(4) = 0.0;
      result.at(5) = 0.0;
    }
  return result;
}

vector <double> DEFAULT_SYSTEM::r2(vector <double> *ya,
				   vector <double> *yb) 
{
  vector <double> result;
  result.push_back(ya->at(1));
  result.push_back(yb->at(1));
  result.push_back(ya->at(3));
  result.push_back(yb->at(3) - 1.0);
  result.push_back(ya->at(4));
  result.push_back(yb->at(4));
  return result;
}

/*
  some default variables
 */

double DEFAULT_SYSTEM::inf_well_V(double *x)
{
  if (*x < 0)
    return numeric_limits<double>::infinity();
  else if ( *x <= 1.0)
    return 0.0;
  else
    return numeric_limits<double>::infinity();
}

double DEFAULT_SYSTEM::parabola(double *x)
{ 
  return pow(*x,2);
}

double DEFAULT_SYSTEM::linear_V(double *x)
{
  if (*x < 0)
    return numeric_limits<double>::infinity();
  else 
    return *x;
}

/*
  switch between internal functions
 */

void DEFAULT_SYSTEM::set_potential( double (DEFAULT_SYSTEM::*V_out)(double *))
{
  V = V_out;
}

/*
  use external function as potential
 */

void DEFAULT_SYSTEM::set_potential( double (*V_out)(double *))
{
  V_ext = V_out;
  V = &DEFAULT_SYSTEM::eval_potential;
}

double DEFAULT_SYSTEM::eval_potential( double *x)
{
  return V_ext(x);
}










