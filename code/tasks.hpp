#ifndef _TASKS_H_
#define _TASKS_H_

#include <vector>
#include <cmath>
#include <string>

#include "functional.hpp"
#include "vector_operators.hpp"
#include "method_tests.hpp"

void select_run_task(char *prog_name, int n);
void print_out_run_task_info(char *prog_name);

// functions of the second problem
void runtask2();
double harm_V(double *x);

// functions of third problem
void runtask3();
vector <double> find_energy_eigenvalues(FUNCTIONAL *first_func,
					 double (*start_value_chooser)
					 (FUNCTIONAL *,
					  FUNCTIONAL *,
					  FUNCTIONAL *,
					  vector <double> *,
					  vector <double> *,
					  vector <double> *,
					  vector <double> *_est,
					  vector <double> *,
					  int ),
					string datafile,
					int n_eig);
double task3_V0(double *x);

double task3_start_value_chooser(FUNCTIONAL *first_func,
				 FUNCTIONAL *func1,
				 FUNCTIONAL *func2,
				 vector <double> *p1,
				 vector <double> *p2,
				 vector <double> *x,
				 vector <double> *x_est,
				 vector <double> *f,
				 int i);

double task3_start_value_chooser_energy(FUNCTIONAL *first_func,
					FUNCTIONAL *func1,
					FUNCTIONAL *func2,
					vector <double> *p1,
					vector <double> *p2,
					vector <double> *x,
					vector <double> *x_est,
					vector <double> *f,
					int i);
// functions of third problem
void runtask4();
double task4_start_value_chooser(FUNCTIONAL *first_func,
				 FUNCTIONAL *func1,
				 FUNCTIONAL *func2,
				 vector <double> *p1,
				 vector <double> *p2,
				 vector <double> *x,
				 vector <double> *x_est,
				 vector <double> *f,
				 int i);

double task4_start_value_chooser_energy(FUNCTIONAL *first_func,
					FUNCTIONAL *func1,
					FUNCTIONAL *func2,
					vector <double> *p1,
					vector <double> *p2,
					vector <double> *x,
					vector <double> *x_est,
					vector <double> *f,
					int i);

double task4_V0(double *x);

#endif /* _TASKS_H_ */
