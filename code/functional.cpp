//====================================================================
//                
// Filename:      functional.cpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Sat May 25 16:31:24 2013
// Modified at:   Tue Jun  4 20:09:40 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   inherent functions (constructor etc) of storage
//                class
// Update count:  0
//                
//====================================================================

#include "functional.hpp"
#include "solve_les.hpp"
#include "vector_operators.hpp"

/*
  default constructor
 */
FUNCTIONAL::FUNCTIONAL ()
{
  this->set_multiple_shooting();
  detect_singularities = false;
  this->set_broyden_steps(0);
  grow_xk = 0; //don't addadditional nodes

  iteration_datafile = NULL;
  results_datafile = NULL;
  system = NULL;
  free_system = false;

  n_iter = 0;
  finished = false;
}
/*
  default parameters for a system of n equations in f
 */
FUNCTIONAL::FUNCTIONAL (int n)
{
  // load default contructor
  this->set_multiple_shooting();
  detect_singularities = false;
  this->set_broyden_steps(0);

  iteration_datafile = NULL;
  results_datafile = NULL;

  n_iter = 0;
  finished = false;
 
  grow_xk = 1; // allow additional nodes

  // set up the default system (see 'default_system.cpp')
  system = new DEFAULT_SYSTEM;
  free_system = true;
  this->f();
  this->r();
  
  eps_tol = 1.0e-8;
  h_start = 1.0e-4;

  eps_s.resize(n,1.0e-4);

  // load estimator mask (only value y(x) from estimator)
  estimator.weight_factor.resize(n,0.0);
  estimator.weight_factor.at(1) = 1.0;
  
  // set default integration region
  xk.resize(2,0.0);
  sk_start.resize(2*n,0.0);
  xk.at(1) = 1.0;
  
}

/*
  allocate a new functional with an old one -> old functional is set as
  estimator for the new one
 */

FUNCTIONAL::FUNCTIONAL (FUNCTIONAL *old_func)
{
  int n=old_func->eps_s.size();
  
  // load default contructor
  if (old_func->multi)
    this->set_multiple_shooting();
  else
    this->set_single_shooting();
  
  detect_singularities = old_func->detect_singularities;
  this->set_broyden_steps(0);

  iteration_datafile = NULL;
  results_datafile = NULL;
  
  n_iter = 0;
  finished = false;
 
  grow_xk = 1; // allow additional nodes
  
  system = new DEFAULT_SYSTEM;
  free_system = true;

  // copy function pointer ("tested" - may fail from time to time)
  f_ext = old_func->f_ext; // function-pointer to real function
  r_ext = old_func->r_ext;
  f_sys = old_func->f_sys; // function-pointer to class-function
  r_sys = old_func->r_sys;
  ptr2f = old_func->ptr2f; // function-pointer called for execution
  ptr2r = old_func->ptr2r;
  system->V_ext = old_func->system->V_ext;
  system->V = old_func->system->V; // potential for DEFAULT_SYSTEM
  
  estimator.set_old_funcitonal(old_func);
  
  eps_tol = 1.0e-8;
  h_start = 1.0e-4;

  eps_s.resize(n,1.0e-4);

  // load estimator mask (only value y(x) from estimator)
  estimator.weight_factor.resize(n,0.0);
  estimator.weight_factor.at(1) = 1.0;
    
  // copy nodes from previous run
  xk = old_func->xk;
  sk_start = old_func->sk_end;
  
}


// and destructor
FUNCTIONAL::~FUNCTIONAL ()
{
  if (iteration_datafile != NULL)
    {
      iteration_datafile->close();
      delete iteration_datafile;
    }

  if (results_datafile != NULL)
    {
      results_datafile->close();
      delete results_datafile;
    }

  if ( free_system == true)
    delete system;
    
}

/*
  set function pointer to compute F_s according to method
*/

void FUNCTIONAL::set_multiple_shooting()
{
  multi = true;
  compute_F_s = &FUNCTIONAL::multi_F_s;
  estimator.multi = true;
}
void FUNCTIONAL::set_single_shooting()
{
  multi = false;
  compute_F_s = &FUNCTIONAL::single_F_s;
  estimator.multi = false;
}


// add broyden steps per newton iteration
void FUNCTIONAL::set_broyden_steps(int steps)
{
  broyden_steps = steps + 1;
}

/*
  take current solution and 
 */
vector <double> FUNCTIONAL::get_solution_value(double *x, vector<double> *y)
{
  vector <double> xi,yi0,yi1,result(*y);
  vector <double>::iterator xii;
  if (! finished)
    {
      finished = true;
      get_solution_iterator = xk.begin();
    }

  if (multi)
    {
      // find last node before desired x value
      if (*get_solution_iterator < *x && get_solution_iterator != xk.end())
	{
	  do
	    {
	      get_solution_iterator++;
	    } while ( *get_solution_iterator < *x
		      && get_solution_iterator != xk.end());
	  get_solution_iterator--;
	}
      else if (*get_solution_iterator > *x && get_solution_iterator != xk.begin())
	do
	  {
	    get_solution_iterator--;
	  } while (*get_solution_iterator > *x
		   && get_solution_iterator != xk.begin());
    }

  // give back the boundary values if the request x lays outside the previos
  // competed area
  if (*x <= xk.front())
    yi1.insert(yi1.end(), sk_end.begin(), sk_end.begin() + eps_s.size());
  else if (*x >= xk.back() && multi )
    yi1.insert(yi1.end(), sk_end.end()-eps_s.size(), sk_end.end());
  else
    {
      // compute from found last node the solution to the desired point
      int i = get_solution_iterator - xk.begin();
      // set up integral to desired point
      xi.push_back(*(get_solution_iterator));
      xi.push_back(*x);
      yi0.insert(yi0.end(), sk_end.begin() + eps_s.size()*i,
		 sk_end.begin() + eps_s.size()*(i+1) );
      
      xii = xi.begin();
  
      yi1 = compute_ivp(&yi0, &xii, NULL, NULL, false);

      // insert just computed value as new node
      xk.insert(get_solution_iterator+1, *x);
      sk_end.insert(sk_end.begin() + eps_s.size()*(i+1),
		    yi1.begin(), yi1.end());

      // update iterator
      get_solution_iterator = xk.begin() + i + 1;
    }

  // estimated system may have different number of variables (provided by y)
  for (unsigned int j = 0; j < yi1.size() && j < result.size(); j++)
    result.at(j) = yi1.at(j);

  return result;
}

/*
  idea: increase x limit until energy eigenvalue is converged
    - use l=0 to increase lower limit (yes, assuming a negative value)
    - use l=1 to increase upper limit ( and here positive...)
 */

void FUNCTIONAL::find_infinite_value(vector <double> *x, int l)
{
  FUNCTIONAL second_func(this), *ptr1, *ptr2, *tptr;

  ptr2 = &second_func;
  ptr1 = this;

  int n = eps_s.size();

  // 
  vector <double> p;
  p.push_back(0.0);
  p.push_back(1.0);
  p.push_back(1.0);

  vector <string> sign;
  sign.push_back("negative");
  sign.push_back("positive");

  int i=0;
  cout << "Converge energy eigenvalue to "
       << sign.at(l) <<  " infinity: ";
  do
    {
      cout.flush();
      ptr2->estimator.set_old_funcitonal(ptr1,p);

      // move boundary
      x->at(l) *= 1.1;

      // reset equation system
      ptr2->xk = *x;
      ptr2->sk_start = ptr2->estimator.get_starting_guess(x,n);
      ptr2->grow_xk = 1;
      ptr2->finished = false;
     
      ptr2->solve_les();  

      // exchange this and second_func pointer
      tptr = ptr1;
      ptr1 = ptr2;
      ptr2 = tptr;

      i++;
      cout << ".";

      // unfortunately there are some fluctuation
      // choosing limits smaller than 1e-6 may prevent 'convergence'
    } while (abs(ptr1->sk_end.at(2)
		 - ptr2->sk_end.at(2)) > 1e-6 && i < 20);

  if (i >= 20)
    cout << "WARNING: no energy convergence towards infinity!" << endl;
  
  x->at(l) /= 1.1;
    
  
  cout << " done!" <<endl;
  return;
}











