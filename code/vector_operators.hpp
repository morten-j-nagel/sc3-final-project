//====================================================================
//                
// Filename:      vector_operators.hpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Wed May 22 13:22:41 2013
// Modified at:   Mon Jun  3 18:39:19 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   header-file with all kinds of vector operators
//                !! IMPORTANT: Matrices are stored in 1-d vectors !!
//                (created for all the exercises (and therefore
//                messy...) )
// Update count:  0
//                
//====================================================================

#ifndef VECTOR_OPERATORS_HPP
#define VECTOR_OPERATORS_HPP

#include <cmath>
#include <vector>
#include <iostream>
#include "vector_operators.hpp"
using namespace std;

/*

  used vector operations - moved to an extra file for reuse
  copied from old exercise

  All matrices are stored as 1d vectors!
  (reason: using lapack... )

 */

// fill a vector from stdin
template <typename T>
void read_vector(vector <T> *v)
{
  for (typename vector <T>::iterator i = v->begin(); i != v->end(); ++i)
    cin >> *i;
}

//write vector to stdout
template <typename T>
void write_vector(vector <T> *v)
{
  cout<< "[";
  for (typename vector <T>::iterator i = v->begin(); i != v->end(); ++i)
    cout << *i<<" ";
  cout<<"]";
}

//write vector to stdout
template <typename T>
void write_matrix(vector <T> *v)
{
  cout<< "[";
  for (typename vector <T>::iterator i = v->begin(); i != v->end(); ++i)
    {
      if ((i - v->begin()) % int(sqrt(v->size())) == 0
	  && i != v->begin())
	cout << endl << " ";
      cout << *i<<" ";
    }
  cout<<"]"<<endl;
}

// overloaded "*" for vector multiplication (sclar-product)
template <typename T>
T operator* (const vector<T> &lhs, const vector<T> &rhs)
{
  if (lhs.size() != rhs.size())
    cout << "Cannot multiply arrays of different size!";
      
  T result = 0;

  for (int i=0; i<lhs.size(); i++)
    result += lhs.at(i)*rhs.at(i);

  return result;
}

// overloaded "|" for vector multiplication (element-wise)
template <typename T>
const vector<T> operator| (const vector<T> &lhs, const vector<T> &rhs)
{
  if (lhs.size() != rhs.size())
    cout << "Cannot multiply piece-wise arrays of different size!";
  
  vector <T> result(rhs);

  for (int i=0; i<lhs.size(); i++)
    result.at(i) = lhs.at(i)*rhs.at(i);

  return result;
}

// overloaded "&" as matrix*vector operator
template <typename T>
const vector<T> operator& (const vector<T> &lhs, const vector<T> &rhs)
{
  // matrix vector
  if (lhs.size() == rhs.size()*rhs.size())
    {
      vector <T> result(rhs);
  
      for (int i=0; i<rhs.size(); i++)
	{
	  vector <T> lhs_row(lhs.begin()+i*rhs.size()
			     ,lhs.begin()+rhs.size()*(i+1));

	  result.at(i) = lhs_row*rhs;
	}

      return result;
    }
  // matrix - matrix
  else if (lhs.size() == rhs.size())
    {
      int n=sqrt(rhs.size());
      vector <T> result(n*n,0);

      for (int i=0; i<n; i++)
	for (int j=0; j<n; j++)
	  for (int l=0; l<n; l++)
	    {
	      result.at(n*i+j) += lhs.at(n*i+l)*rhs.at(n*l+j);
	    }
      
      return result;
    }
  else 
    {
      cout << "Cannot do a matrix-(vector|matrix) multiplication with these dimensions";
      return vector<T>(lhs.size(),-1);
    }
}

// overloaded "%" as vector*vector (outer product)
template <typename T>
const vector<T> operator% (const vector<T> &lhs, const vector<T> &rhs)
{
  vector <T> result(rhs.size()*lhs.size());
  
  for (int i=0; i<lhs.size(); i++)
    {
      for (int j=0; j<rhs.size(); j++)
	result.at(i*rhs.size()+j) = lhs.at(i)*rhs.at(j);
    }

  return result;
}

// overloaded "-" as vector - vector (element wise)
template <typename T>
const vector<T> operator- (const vector<T> &lhs, const vector<T> &rhs)
{
  if (lhs.size() != rhs.size())
    cout << "Cannot substract arrays of different size!";

  vector <T> result(rhs.size());
  
  for (int i=0; i<lhs.size(); i++)
    result.at(i) = lhs.at(i) - rhs.at(i);
    
  return result;
}

// overloaded "+" as vector + vector (element wise)
template <typename T>
const vector<T> operator+ (const vector<T> &lhs, const vector<T> &rhs)
{
  if (lhs.size() != rhs.size())
    cout << "Cannot substract arrays of different size!";

  vector <T> result(rhs.size());
  
  for (int i=0; i<lhs.size(); i++)
    result.at(i) = lhs.at(i) + rhs.at(i);
    
  return result;
}

template <typename T>
vector<T> operator/ (const vector<T> &lhs, T rhs)
{
  vector <T> result(lhs);
  for (typename vector <T>::iterator i = result.begin(); i != result.end(); ++i)
    {
      *i /= rhs;
    }

  return result;
}

template <typename T>
vector<T> operator* (const vector<T> &lhs, T rhs)
{
  vector <T> result(lhs);
  for (typename vector <T>::iterator i = result.begin(); i != result.end(); ++i)
    {
      *i *= rhs;
    }

  return result;
}

template <typename T>
void add_column(vector <T> *m, vector <T> *v, int l)
{
  if (m->size() != pow(v->size(),2))
    m->resize(pow(v->size(),2));

  if ( !(l < v->size()) )
    {
      cout << "Out of bound - cannot add vector to matrix" << endl;
      return;
    }
  
  for (typename vector <T>::iterator i = v->begin();  i != v->end(); ++i)
    {
      *(m->begin() + (i - v->begin())*v->size() + l) = *i;
    }
}


template <typename T>
T l2_norm(vector <T> v)
{
  return sqrt(v*v);
}

template <typename T>
T l1_norm(vector <T> v)
{
  T result  = 0.0;
  for (typename vector <T>::iterator i = v.begin(); i != v.end(); ++i)
    result += *i;

  return result;
}

template <typename T>
T max_norm(vector <T> v)
{
  T result = 0.0;
  for (typename vector <T>::iterator i = v.begin(); i != v.end(); ++i)
    {
      if (abs(*i)> result)
	result = abs(*i);
    }

  return result;
}


// fortran layout of arrays is different from C array layout (rows and columns 
// exchanged).
// recycling old function from exercise 2 - problem04
template <typename Type>
void transpose_matrix(Type *A)
{
  // from http://stackoverflow.com/questions/2566675
  int n2 = A->size();
  int n = sqrt(n2);
  int q = n2 - 1;
  int k,i=0;

  do 
    {
      k = (i*n) % q;
      while (k>i) 
	k = (n*k) % q;
      if ( i!=k ) 
	swap(A->at(i),A->at(k));
	
    } while (++i <= (n2-2));
}

// return in a vector the diagonal elements of an matrix
template <typename T>
vector <T> diag(vector <T> *A)
{
  int n;
  n = sqrt(A->size());

  if (n*n != A->size())
    cout << "Cannot compute diagonal elements of an nonsquare matrix" << endl;

      vector <T> result(n);

  // get the diagonal elements
  for (int i=0; i<n; i++)
    result.at(i) = A->at(i*(n+1));

  return result;
}

template <typename T>
void add_block_to_matrix(vector <T> *m,
			 vector <T> *block,
			 int i_start, int i_end,
			 int j_start, int j_end)
{
  if (std::abs((double)(1+i_end - i_start)*(1+j_end - j_start)) != block->size() )
    {
      cout << "Not a proper block - wrong dimensions" << endl;
      return;
    }

  int n_m = sqrt(m->size());
  int n_block = sqrt(block->size());
  int j_shift=j_start*n_m;

    
  for (int i=0; i<block->size(); i++)
    {
      m->at(i_start+i + j_shift) = block->at(i);
      if ( (i+1)%n_block == 0)
	j_shift += n_m - n_block;
    }
}

template <typename T>
void grow_matrix(vector <T> *m,int add_col_row)
{
  
  if (add_col_row <= 0)
    {
      cout << "GROW_matrx - no shrinking implemented!" << endl;
      return;
    }
  vector <T> result;
  int n = sqrt(m->size());
  
  if (pow(n,2) != m->size() )
    {
      cout << "Cannot grow none-square matrix!" << endl;
      cout << pow(n,2) << " != " << m->size() << endl;
      return;
    }

  // create empty(=filled with zeros) matrix
  result.resize(pow(n+add_col_row,2),0.0);

  // add the old matrix as block to the new one
  add_block_to_matrix(&result,m,0,n-1,0,n-1);

  *m = result;
  return;
}

// create a unity matrix of prefered size
template <typename T>
void unity(vector <T> *m, int size)
{
  m->resize(size*size,0);
  for (int i=0; i<size; i++)
    m->at(i+i*size) = 1.0;
}

#endif















