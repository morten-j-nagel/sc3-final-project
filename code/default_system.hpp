#ifndef _DEFAULT_SYSTEM_H_
#define _DEFAULT_SYSTEM_H_

#include <vector>
#include <cmath>
#include <limits>

using namespace std;

class DEFAULT_SYSTEM
{
  friend class FUNCTIONAL;
private:
  double (*V_ext)(double *);

  double eval_potential( double *x);
public:
  DEFAULT_SYSTEM();
  /*
    virtual ~DEFAULT_SYSTEM();
  */
  double (DEFAULT_SYSTEM::*V)(double *x);

  // a.u. Schroedinger eq. with fixed right side
  vector <double> f(double *x,vector <double> *y);
  vector <double> r(vector <double> *ya,
		    vector <double> *yb) ;
  
  // a.u. Schroedinger eq. with free right side
  vector <double> f2(double *x,vector <double> *y);
  vector <double> r2(vector <double> *ya,
		     vector <double> *yb) ;
  
  // default potentials
  double inf_well_V(double *x);
  double parabola(double *x);
  double linear_V(double *x);

  // manipulate potential
  void set_potential( double (*V_out)(double *));
  void set_potential( double (DEFAULT_SYSTEM::*V_out)(double *));

};

#endif /* _DEFAULT_SYSTEM_H_ */


