//====================================================================
//                
// Filename:      tasks.cpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Mon Jun  3 15:25:12 2013
// Modified at:   Fri Jun  7 22:20:19 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   functions to compute the taks given by the lecturer
// Update count:  0
//                
//====================================================================

#include "tasks.hpp"

// global variable used to scale potentials in task 3 and 4
double task3_scale=1.0;
double task4_scale=1.0;

// select wished task
void select_run_task(char *prog_name, int n)
{
  switch (n)
    {
    case 0: select_run_test(prog_name,0); runtask2(); runtask3(); runtask4();
      break;
    case 1: select_run_test(prog_name,0);
      break;
    case 2: runtask2();
      break;
    case 3: runtask3();
      break;
    case 4: runtask4();
      break;
    default: print_out_run_task_info(prog_name);
    }
  
  return;
}

// some output for user guidance

void print_out_run_task_info(char *prog_name)
{
  cout << "Run with '" << prog_name << " N' to compute following tasks: "<< endl
       << "  0: selects all tasks" << endl
       << "     (see below)" << endl
       << "  1: run all tests define in 'method_tests.cpp' " << endl
       << "     ( and test the implementation this way)" << endl
       << "  2: solve 1D Schrödinger eq. for infinite well"
       << " and a harmonic potential" << endl
       << "     ( to analyse use gnuplot script 'task_2.glt' )" << endl
       << "  3: solve 1D Schrödinger eq. with V(x) = x>=0 ? x : Inf" << endl
       << "     ( computer the first 10 energy eigenvalues )" << endl
       << "  4: solve 1D Schrödinger eq. with V(x) = x>=0 ? sqrt(x) : Inf " << endl
       << "     ( computes the first 10 energy eigenvalues )" << endl << endl;
}

/*
  Using atomic units compute the following subtasks
  
  task A:
    electron in infinite well (x0 = 0, x1=1)

  task B:
    electron in harmonic potential V(x) = x^2/2 (assuming ω=1)
    
  see 'task_2.glt' which compares result to the analytic solution
 */

void runtask2()
{
  cout << "============================================================" << endl
       << "Computing task 2 ... " <<endl << endl;

  cout << "Task a: infinite potential well" << endl <<endl;
  
  vector < double > f_a,x_a;

  // choose default system
  FUNCTIONAL func_a(4);

  // initialize estimator - use just sin(x) 
  f_a.push_back(1.0);
  f_a.push_back(1.0);

  x_a.push_back(0.0);
  x_a.push_back(1.0);

  func_a.estimator.set_sin(f_a,x_a);
  
  // find first eigenvalue
  func_a.xk = x_a;
  func_a.sk_start = func_a.estimator.get_starting_guess(&x_a,4);
  
  //func_a.print_iterations_to_file("data/task2-infinite-well-iter.dat");

  func_a.solve_les();

  func_a.print_run_information();

  func_a.print_result_to_file("data/task2-infinite-well.dat",true, 0.01);
  int n_eign=5;

  // search for 5 more solutions
  for (int i=2; i<=n_eign; i++)
    {
      // allocate estimator to nest sin-curve
      // and reset run
      func_a.grow_xk = 1;
      f_a.at(1) = i;
      func_a.estimator.set_sin(f_a,x_a);
      func_a.sk_start = func_a.estimator.get_starting_guess(&x_a,4);
      func_a.xk = x_a;
      func_a.finished = false;

      func_a.solve_les();

      func_a.print_run_information();
      func_a.print_result_to_file("data/task2-infinite-well.dat",true,0.01);
    }

  
  
  cout << "done!" << endl
       <<"Check: 'data/task2-infinite-well.dat' for generate solutions"
       << endl << endl;

  cout << "Task b: harmonic oscillator" <<endl << endl;
  // almost make_run() with strip comments
  FUNCTIONAL funcs(4);

  funcs.system->set_potential(harm_V);
  
  vector <double> x,f,x_est;
  f.push_back(0.7); // normalizing
  f.push_back(1.0); // choose n - plot above with n=5

  // inner region
  x_est.push_back(-2.0);
  x_est.push_back(2.0);
  // starting guess for intervall where phi(x) might be none zero
  x.push_back(-3.0);
  x.push_back(3.0);

  funcs.estimator.max_relative_error = 0.2;
  funcs.estimator.min_absolute_error = 0.2;
  funcs.estimator.max_delta_x = 1.0;
  
  funcs.estimator.weight_factor.resize(4,0.0);
  funcs.estimator.weight_factor.at(0) = 0.0;
  funcs.estimator.weight_factor.at(1) = 1.0;

  for (int n=0; n < 5; n++)
    {
      f.at(1) = n + 1.0;
      funcs.estimator.set_sin_cut2(f,x_est);
      funcs.finished = false;
      funcs.grow_xk = 1;
      funcs.xk = x;
      funcs.sk_start = funcs.estimator.get_starting_guess(&x,4);
      
      // do the shooting ;)
      funcs.solve_les();
      
      funcs.print_run_information();
  
      // compute infinite values
      funcs.find_infinite_value(&x,0);
      funcs.find_infinite_value(&x,1);

      funcs.print_run_information();

      funcs.print_result_to_file("data/task2-harmonic.dat",true,0.01);
    }  
  return;
}

double harm_V(double *x)
{
  return 0.5*pow(*x,2.0);
}

/*
  Solve the Schroedinger Equation for the potential 'V = x >=0 ? x : inf '
 */

void runtask3()
{
  cout << "============================================================" << endl
       << "Computing task 3 ... " <<endl <<endl;
  
  FUNCTIONAL funcs(4);
  vector <double> energies;

  // choose default system - set wished potential
  funcs.system->set_potential(task3_V0);

  // set some estimator variables
  funcs.estimator.max_relative_error = 0.2;
  funcs.estimator.min_absolute_error = 0.1;
  funcs.estimator.max_delta_x = 0.5;
  funcs.estimator.weight_factor.resize(4,1.0);
  funcs.estimator.weight_factor.at(2) = 0.0;
  funcs.estimator.weight_factor.at(3) = 0.0;

  // see below
  cout << "Compute first 10 energy eigenvalues: "<< endl << endl;
  energies = find_energy_eigenvalues(&funcs,
  				     task3_start_value_chooser,
  				     "data/task3_find_energy_eigenvalue.dat",
  				     10);

  cout << "Found energies: " << endl << " ";
  write_vector(&energies);
  cout << endl << endl;

  funcs.close_print_result_to_file();
  
  cout << "Calculate dependence between ground state and potential scaling:"
       << endl << endl;
  energies = find_energy_eigenvalues(&funcs,
				     task3_start_value_chooser_energy,
				     "data/task3_potential_eigenvalue.dat",
				     10);

  cout << "Found energies: " << endl << " ";
  write_vector(&energies);
  cout << endl << endl;
  
  
  cout << "DONE!"<<endl
       << "Check: 'data/task3_{find_energy,potential}_eigenvalue.dat'"
       << " for generate solutions"
       << endl << endl;
  
}

double task3_V0(double *x)
{
  if (*x < 0)
    return numeric_limits<double>::infinity();
  else
    return *x*task3_scale;
}

/*
  generate tailored starting guesses for each eigenvalue - for task 3
 */

double task3_start_value_chooser(FUNCTIONAL *first_func,
				 FUNCTIONAL *func1,
				 FUNCTIONAL *func2,
				 vector <double> *p1,
				 vector <double> *p2,
				 vector <double> *x,
				 vector <double> *x_est,
				 vector <double> *f,
				 int i)
{
  if (i==1)
    {
      // set sinus estimator 
      f->at(0) = 1.5; // normalizing
      f->at(1) = 1.0; // choose n=1
	  
      x_est->at(0) = 0.0;
      x->at(0) = 0.0;
      x_est->at(1) = 2.0;
      x->at(1) = 4.0;
      first_func->estimator.set_sin_cut(*f,*x_est);
    }
  else
    {
      // combine from last solution and the ground state an estimator
      // for the next energy eigenvalue
      p2->at(0) += 0.9;
      if (i==2)
	p2->at(0) += 0.3;
      p2->at(2) *= -1.0;
      p2->at(1) = 0.9;

      first_func->estimator.set_two_funcitonal(func2,*p2,func1,*p1);
    }
  // ensure potential is unscaled
  task3_scale = 1.0;
  
  // guessed energy increase
  return 0.6;
}


double task3_start_value_chooser_energy(FUNCTIONAL *first_func,
					FUNCTIONAL *func1,
					FUNCTIONAL *func2,
					vector <double> *p1,
					vector <double> *p2,
					vector <double> *x,
					vector <double> *x_est,
					vector <double> *f,
					int i)
{
  // scale potential
  task3_scale += 1.0;
  
  // set sinus estimator 
  f->at(0) = 1.5; // normalizing
  f->at(1) = 1.0; // choose n=1
  
  x_est->at(0) = 0.0;
  x->at(0) = 0.0;
  x_est->at(1) = 2.0;
  x->at(1) = 4.0;
  first_func->estimator.set_sin_cut(*f,*x_est);
    
  
  // guessed energy increase
  return 0.6;
}

/*
  Solve the Schroedinger Equation for the potential 'V = x >=0 ? x : inf '
 */

void runtask4()
{
  cout << "============================================================" << endl
       << "Computing task 4 ... " <<endl <<endl;
  
  FUNCTIONAL funcs(4);
  vector <double> energies;

  // choose default system - set wished potential
  funcs.system->set_potential(task4_V0);

  // set some estimator variables
  funcs.estimator.max_relative_error = 0.2;
  funcs.estimator.min_absolute_error = 0.2;
  funcs.estimator.max_delta_x = 0.5;
  funcs.estimator.weight_factor.resize(4,0.0);
  funcs.estimator.weight_factor.at(1) = 1.0;

  // see below
  cout << "Compute first 10 energy eigenvalues: "<< endl << endl;
  energies = find_energy_eigenvalues(&funcs,
  				     task4_start_value_chooser,
  				     "data/task4_find_energy_eigenvalue.dat",
  				     10);

  cout << "Found energies: " << endl << " ";
  write_vector(&energies);
  cout << endl <<endl;

  funcs.close_print_result_to_file();

  cout << "Calculate dependence between ground state and potential scaling:"
       << endl << endl;
  energies = find_energy_eigenvalues(&funcs,
				     task4_start_value_chooser_energy,
				     "data/task4_potential_eigenvalue.dat",
				     10);
  
  cout << "Found energies: " << endl << " ";
  write_vector(&energies);
  cout << endl << endl;
  
  cout << "DONE!"<<endl
       << "Check: 'data/task4_{find_energy,potential}_eigenvalue.dat'"
       << "for generate solutions"
       << endl << endl;
  
}

/*
  generate tailored starting guesses for each eigenvalue - for task 4
 */

double task4_start_value_chooser(FUNCTIONAL *first_func,
				 FUNCTIONAL *func1,
				 FUNCTIONAL *func2,
				 vector <double> *p1,
				 vector <double> *p2,
				 vector <double> *x,
				 vector <double> *x_est,
				 vector <double> *f,
				 int i)
{
  if (i==1)
    {
      // set sinus estimator 
      f->at(0) = 0.5; // normalizing
      f->at(1) = 1.0; // choose n=1
	  
      x_est->at(0) = 0.0;
      x->at(0) = 0.0;
      x_est->at(1) = 3.0;
      x->at(1) = 7.0;
      first_func->estimator.set_sin_cut(*f, *x_est);
    }
  else
    {
      // combine from last solution and the ground state an estimator
      // for the next energy eigenvalue
      p2->at(0) += 1.7;
      p2->at(2) *= -1.05;
      p2->at(1) = 0.9;

      first_func->estimator.set_two_funcitonal(func2,*p2,func1,*p1);
    }

  // ensure potential is scled by one
  task4_scale = 1.0;
  
  // guessed energy increase
  return 0.2;
}

double task4_start_value_chooser_energy(FUNCTIONAL *first_func,
					FUNCTIONAL *func1,
					FUNCTIONAL *func2,
					vector <double> *p1,
					vector <double> *p2,
					vector <double> *x,
					vector <double> *x_est,
					vector <double> *f,
					int i)
{
  // raise factor a in front of the square-root
    
  task4_scale += 1.0;
  
  // set sinus estimator 
  f->at(0) = 0.5; // normalizing
  f->at(1) = 1.0; // choose n=1

  x_est->at(0) = 0.0;
  x->at(0) = 0.0;
  x_est->at(1) = 3.0;
  x->at(1) = 7.0;
  first_func->estimator.set_sin_cut(*f, *x_est);


  // guessed energy increase
  return 0.2;
}

double task4_V0(double *x)
{
  if (*x < 0)
    return numeric_limits<double>::infinity();
  else
    return task4_scale*sqrt(*x);
}

/*

  find 'n_eig' different energy eigenvalues

  Each results is obtained by a combination of the last result and the
  ground-state.  To get 'infinity' value the shooting interval is
  increased until the energy eigenvalue is converged.

  (used both for task 3 and task 4)
 */

vector <double>  find_energy_eigenvalues(FUNCTIONAL *first_func,
					 double (*start_value_chooser)
					 (FUNCTIONAL *,
					  FUNCTIONAL *,
					  FUNCTIONAL *,
					  vector <double> *,
					  vector <double> *,
					  vector <double> *,
					  vector <double> *_est,
					  vector <double> *,
					  int ),
					 string datafile,
					 int n_eig)
{
  // energy eigenvalues
  vector <double> energy_eigenvalues;
  
  // func1 and func2 are not really compute but act as storage for
  // different solution of first_func.
  FUNCTIONAL *func1=NULL,*func2=NULL;

  // f,x_est - parameters for initial estimator
  // x - shooting intervall
  // s_start - starting value
  // p1,p2 - oarameters for combining solutions
  vector < double > f,x,x_est, s_start;
  vector <double> p1, p2;
  
  int n = first_func->eps_s.size();

  s_start.resize(n,0.0);
  
  double start_enery = 0.0;
  double factor = 1.0;
  
  f.resize(2,0.0);
  x = f;
  x_est = f;

  // set scaling and shifting parameter for estimator from old
  // functionals
  p1.resize(3,1.0);
  p1.at(0) = 0.0;
  p2 = p1;
  
  for (int i=1; i<= n_eig; i++)
    {
      factor = start_value_chooser(first_func,func1,func2,
				   &p1, &p2, &x, &x_est, &f, i);
      do
	{
	  // reset equation system
	  first_func->xk = x;

	  s_start.at(2) += factor;
	  if (i == 1)
	    first_func->sk_start = first_func->estimator.get_starting_guess(&x,&s_start);
	  else
	    {
	      s_start.at(2+n) += factor;
	      first_func->sk_start = s_start;
	    }
	  first_func->grow_xk = 1;
	  first_func->finished = false;
	  
	  first_func->solve_les();  

	} while (abs(first_func->sk_end.at(2) - start_enery) < first_func->eps_tol );
      
      // check for positive infinite convergence
      first_func->find_infinite_value(&x,1);

      first_func->print_run_information();
      
      // write results
      first_func->print_result_to_file(datafile,true);

      // save result and setup next run
      energy_eigenvalues.push_back(first_func->sk_end.at(2));
      
      s_start.assign(first_func->sk_end.begin(), first_func->sk_end.begin()+8);
      
      // create copies to use later as estimator
      if (i==1)
	{
	  // two times the ground-state
	  func1 = new FUNCTIONAL(first_func);
	  func2 = new FUNCTIONAL(first_func);
	  // copy solution
	  func1->sk_end = first_func->sk_end;
	  func2->sk_end = first_func->sk_end;
	}
      else
	{
	  // replace one groundstate with last solution
	  delete func1;
	  func1 = new FUNCTIONAL(first_func);
	  func1->sk_end = first_func->sk_end;
	}
    }

  if (func1 != NULL)
    delete func1;
  if (func2 != NULL)
    delete func2;
  
  return energy_eigenvalues;
}
