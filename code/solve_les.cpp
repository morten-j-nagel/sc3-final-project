//====================================================================
//                
// Filename:      solve_les.cpp
// Author:        Morten Nagel <morten.nagel@helsinki.fi>
// Created at:    Wed May 22 14:39:35 2013
// Modified at:   Fri Jun  7 10:56:47 2013
// Modified by:   Morten Nagel <morten.nagel@helsinki.fi>
//                
// Description:   contains equation system solver to get optimal
//                shooting parameters
// Update count:  0
//                
//====================================================================

#include "solve_les.hpp"

/*

  solve non-linear equation system to get working s

  broyden's method is in principle cheaper (per step) but in the end often
  slower converging (so no real gain).

  the number of broyden iteration per newton step is controlled by the variable
  'broyden_steps'.
  
 */

vector <double> FUNCTIONAL::solve_les()
{
  vector <double> B, y(sk_start), delta_s(sk_start), previous_delta_s(sk_start);
  double lambda = 1.0;

  F_s.resize(sk_start.size(),0.0);
  s = sk_start;
  
  delta_F_s.resize(pow(sk_start.size(),2),0.0);
  B.resize(pow(sk_start.size(),2),0.0);

  n_iter = 0;

  if (finished)
    cout << "WARNING: System is marked as 'finished' and may be used as estimator"
	 << endl << "         - strange things may happen ;)" << endl;
  
  do
    {
      if (n_iter % broyden_steps == 0)
	{
	    /*
	      At first to an 'brute force' newton step
	    */
	  
	  (this->*compute_F_s)(true);
	  B = get_inverse(&delta_F_s);
	}
      else
	{

	  /*
	    
	    And than use Broyden's update method to prevent costly
	    recalculations of the jacobian matrix
	    
	    (recycled from exercies 4 problem 4)
	  */
	 
	  // Broyden algorithmen (a little bit redrafted 
	  // - might change result slightly due to differen numerical errors)
	  // also renaming of x_i -> s and s->-delta_s
	  
	  // calculate new y, but y only need in B*y 
	  // -> therefore to save one matrix-vector product
	  // save y as B*y
	  // => y' = B_i-1y_i
	  // => y' = B*(f(x_i) - f(x_i-1)) = B*f(x_i) + s_i
	  // so only y'' = B*f(x_i) is computed
	  (this->*compute_F_s)(false);
	  y = B&F_s;

	  // update from B_i-1 to B_i
	  // with y'' = y' - s_i = B_i-1*y_i - s_i (change in sign !)
	  // => (y''+s) = B_i-1*y_i
	  // - (s_i - B_i-1*y_i) = -y''
	  // % - outer product
	  // & - matrix-matrix product
	  // * - vector-scalar product
	  B = B - ((y % delta_s) & B )/(delta_s * (y-delta_s));
	}

      previous_delta_s = delta_s;
      
      // calculate s (without x_i and x_i-1 ) old step 2
      // s = x_i - x_i-1 = x_i-1 - B*f(x_i-1) - x_i-1 = - B*f(x_i-1)
      delta_s = B&F_s;

      // try to break cycles
      if (previous_delta_s.size() == delta_s.size() && multi ) {
	if (l2_norm(previous_delta_s + delta_s) < sqrt(eps_tol) &&
	    l2_norm(previous_delta_s - delta_s) > sqrt(eps_tol)){
	  if (lambda*0.9 > 0.1)
	    {
	      lambda *= 0.9;
	    }
	  else
	    {
	      lambda = 0.1;
	      broyden_steps += 10;
	    }
	}}
      // now the original step 1      
      s = s - delta_s*lambda;

      n_iter++;
      
      // check for convergence
      // or if the calculation is broken
      // (::isnan needed to circumvent C++11-gnu bug...)
    } while (l2_norm(delta_s) > eps_tol
	     && ! ::isnan( l2_norm(delta_s) ) );

  sk_end = s;
  
  return F_s;
}

/*
  
  calculated inverse of a matrix using LAPACK-functions
  (recycle exercise 4 problem 4)
  
*/

vector <double> FUNCTIONAL::get_inverse(vector <double> *delta_F_s)
{
  vector <double> inverse(*delta_F_s);

  // transpose to get fortran notation
  transpose_matrix(&inverse);

  // using lapack to get the inverse jacobian
  // following http://stackoverflow.com/questions/3519959

  // some working arrays/ variables
  int N = sqrt(inverse.size());
  int *IPIV = new int[N+1];
  int LWORK = N*N;
  double *WORK = new double[LWORK];
  int INFO;

  // made a LU decomposition
  dgetrf_(&N,&N,&*inverse.begin(),&N,IPIV,&INFO);

  // and calculate the inverse
  dgetri_(&N,&*inverse.begin(),&N,IPIV,WORK,&LWORK,&INFO);

  delete IPIV;
  delete WORK;

  transpose_matrix(&inverse);
  return inverse;
}

/*
  vary each component of s to create DF(s) matrix
 */

void FUNCTIONAL::compute_delta_F_s(vector <double> *block_F_s,
				   vector <double> *block_delta_F_s,
				   vector <double> *block_s,
				   vector <double>::iterator *xk)

{
  vector <double> s_temp(*block_s);
  vector <double> delta_F_s_column(*block_s);

  for (vector <double>::iterator si=eps_s.begin();
       si != eps_s.end(); si++)
    {
      // get a clean copf of current s
      s_temp = *block_s;

      // modifiy one sigma value
      s_temp.at(si - eps_s.begin()) += *si;
      
      // compute all differences for this sigma-value
      delta_F_s_column = (compute_ivp(&s_temp, xk, NULL, NULL,false)
			  - *block_F_s)/ *si;

      // and store it in delta_F_s
      add_column(block_delta_F_s,&delta_F_s_column,si - eps_s.begin());
    }

  return;
}

/*

  compute F_s (minimiyed vector for solution) - single shooting method
  
 */

void FUNCTIONAL::single_F_s(bool bool_delta_F_s)
{
  vector <double>::iterator cur_xk = xk.begin();
  // perform a shot with current s-value

  F_s = compute_ivp(&s,&cur_xk, NULL, iteration_datafile, false);
    
  if (bool_delta_F_s )
    compute_delta_F_s(&F_s, &delta_F_s, &s, &cur_xk);

  if (iteration_datafile != NULL)
    *iteration_datafile << endl;

}


/*

  compute F_s (minimiyed vector for solution) - multiple shooting method
  
 */

void FUNCTIONAL::multi_F_s(bool bool_delta_F_s)
{
  vector <double> block_F_s, block_delta_F_s;
  vector <double> one;
  vector <double> sk;

  vector <double>::iterator cur_xk, cur_sk;
  
  int n_sk = s.size()/(xk.size());

  if (bool_delta_F_s )
    {
      block_delta_F_s.resize(pow(n_sk,2),0.0);

      unity(&one,n_sk);

      one = one*(-1.0);
    }
      
  // empty right-hand side of the equations system
  F_s.resize(0);

  // fill vector iterator
  cur_xk = xk.begin();
  cur_sk = s.begin();

  bool bool_grow_xk=false;
  // set grow_xk during broyden steps and for single shooting method to zero
  // (preventing this way the addition of farther nodes)
  if (grow_xk > 0 && multi && bool_delta_F_s)
    {
      bool_grow_xk = true;
      grow_xk--;
    }
    
  for (unsigned int i=1; i<xk.size(); i++)
    {
      // pick the right s_k value
      sk.assign(cur_sk, cur_sk + n_sk);
      block_F_s = compute_ivp(&sk, &cur_xk, &cur_sk,
			      iteration_datafile, bool_grow_xk);

      F_s.insert(F_s.end(),block_F_s.begin(),block_F_s.end());

      // building the (DF(s))-block-matrix
      if (bool_delta_F_s )
	{
	  compute_delta_F_s(&block_F_s, &block_delta_F_s, &sk,
			    &cur_xk);
	  
	  add_block_to_matrix(&delta_F_s, &block_delta_F_s,
			      (i-1)*n_sk, i*n_sk-1, (i-1)*n_sk, i*n_sk -1);
	  
	  add_block_to_matrix(&delta_F_s, &one,
			      i*n_sk, (i+1)*n_sk-1, (i-1)*n_sk, i*n_sk -1);
	}

      cur_xk++;
      cur_sk += n_sk;
    }

  int i=0;
  for (vector <double>::iterator si = s.begin() + n_sk;
       si != s.end(); si++)
    {
      F_s.at(i) -= *si;
      i++;
    }
  
  // add r line to F_s (last element
  sk.assign(cur_sk, cur_sk + n_sk);
  block_F_s = r(&s,&sk);
  F_s.insert(F_s.end(),block_F_s.begin(),block_F_s.end());

  if (bool_delta_F_s )
    {
      // build the last A and B blocks
      // (lowest block-row of DF(s))
      vector <double> A(one), B(one);

      int n_s = s.size()-1;
  
      compute_delta_F_s_A_B(&A, &B);
      
      add_block_to_matrix(&delta_F_s,&A,
			  0, n_sk-1, n_s-n_sk+1, n_s);
      add_block_to_matrix(&delta_F_s,&B,
			  n_s-n_sk+1, n_s,
			  n_s-n_sk+1, n_s);
    }
  
  if (iteration_datafile != NULL)
    *iteration_datafile << endl;
  
  return;
}

/*
  compute the last blocks in multiple shooting DF(s)
  - influence of varying s on the residual r
 */

void FUNCTIONAL::compute_delta_F_s_A_B(vector <double> *A,
				       vector <double> *B)
{
  vector <double> s_1(eps_s), s_m(eps_s),
    s_1_temp(eps_s), s_m_temp(eps_s);
  vector <double> column, r_1_m;
  
  int n_sk = eps_s.size();

  s_1.assign(s.begin(), s.begin()+n_sk);
  s_m.assign(s.end()-n_sk, s.end());

  r_1_m = r(&s_1,&s_m);
  
  for (vector <double>::iterator si=eps_s.begin(); si != eps_s.end(); si++)
    {
      // get a clean copf of current s
      s_1_temp = s_1;
      s_m_temp = s_m;
      
      // modifiy one sigma value
      s_1_temp.at(si - eps_s.begin()) += *si;
      s_m_temp.at(si - eps_s.begin()) += *si;

      // compute A and B
      column = (r(&s_1_temp, &s_m) - r_1_m)/ *si;
      add_column(A,&column,si - eps_s.begin());
      
      column = (r(&s_1, &s_m_temp) - r_1_m)/ *si;
      add_column(B,&column,si - eps_s.begin());
      
    }

  return;
}











