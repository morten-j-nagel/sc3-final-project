\documentclass[a4paper]{scrartcl}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{bbold}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage{placeins}
\usepackage[format=hang, labelfont=bf, textfont={normalfont}]{caption}
\usepackage[format=hang, labelfont=normalfont, textfont={normalfont},
list=true, skip=0.5ex]{subcaption}
\usepackage{epstopdf}
\usepackage{pdfsync}
\usepackage[pdftex]{hyperref}
\usepackage{url}

\title{Boundary valueproblem: \\ One-dimensional Schrödinger equation}
\author{Morten Nagel}
\date{\today}

\begin{document}
\maketitle

\section{Introduction}
\label{sec:introduction}

This document contains a report about the method used and the results
obtained for the final project of the \textit{scientific computing
  III} course.  The problem to solve was the \textit{time-independent
Schrödinger equation}, introduced in
Sec. \ref{sec:schrodinger-equation}, a \textit{boundary value
  problem.}  The \textit{shooting method} was applied to solve it and
its details are described in Sec.~\ref{sec:numerical-methods}.  The
given tasks and their results are detailed in
section~\ref{sec:tasks}.

\subsection{The Schrödinger equation}
\label{sec:schrodinger-equation}

The \textit{Schrödinger equation} is one of the main equation of
quantum mechanics describing the quantum state of a system.  Its
general for is the one of a wave-equation, but it is for the given
problem simplified to the \textit{time-independent Schrödinger
  equation} for a single non-relativistic electron in atomic units is:
\begin{align}
  \label{eq:1}
  - \frac{1}{2} \frac{d^2 \varPsi(x)}{d x^2} + V(x) \varPsi(x) &= E \varPsi(x)
\end{align}
With it only an electron in a one dimensional system is described and
its general solution are standing waves.  As additional requirement
its solution have to be \textit{normalized}:
\begin{align}
  \label{eq:19}
  \int_{-\infty}^{\infty} |\varPsi(x)|^2 dx = 1
\end{align}
This is derived from the interpretation of $|\varPsi(x)|^2$ as a
probability density for an electron to be at the location x.
Integrating over the whole space yields in total number of present
electrons, which should be one, if a single electron system is
investigated. 

Almost all non-trivial potentials $V(x)$ only a discrete amount of
solutions with a limited amount of energy eigenvalues exist.  As a
consequence electrons have a defined energies and can only absorb or
loose a discrete amount of energy, when trapped in a potential. The
energy of bound electrons are \textit{quantized}.

The solution of \eqref{eq:1} are defined over the whole space and can
be in classically forbidden regions non-zero - explaining the
quantum-mechanically effect of tunnelling, when particles like
electrons move through potential barriers, while lacking the
classically needed energy to overcome the barrier.

For the current problem the only systems investigated are of the kind
\begin{align}
  \label{eq:20}
  V(x) &\rightarrow \infty, \text{if} ~|x| \rightarrow \infty
\end{align}
In these region the Schrödinger equation can only be fulfilled, if
\begin{align}
  \label{eq:21}
  \varPsi(x) &\rightarrow 0, \text{if} ~|x| \rightarrow \infty.
\end{align}
This holds true for a common potential like the \textit{Coulomb
  potential}, which a positive charged Hydrogen-core act upon a
negative charged electron.  As a consequence of \eqref{eq:19} and
\eqref{eq:21} the solutions for this problem show that the electron
can be trapped at hydrogen-core.  


\section{Numerical methods}
\label{sec:numerical-methods}

The system described by the 1-dimensional Schrödinger equation
\eqref{eq:1} with the boundary conditions \eqref{eq:19} and
\eqref{eq:21} can be transformed to the four-dimensional first order
boundary value problem:
\begin{subequations}
  \label{eq:23}
  \begin{align}
    \label{eq:22}
    y'_0 &= 2[V(x) - y_2]y_1 \\
    \label{eq:24}
    y'_1 &= y_0 \\
    \label{eq:25}
    y'_2 &= 0 \\
    \label{eq:26}
    y'_3 &= y_1^2
  \end{align}
\end{subequations}
with
\begin{align}
  \label{eq:27}
  y_0 &= \varPsi'(x) & y_1 &= \varPsi(x) & y_2&=E & y_3&=\int
  |\varPsi(x)|^2 dx \\
  \intertext{and the boundary conditions}
  \label{eq:28}
  y_1(a) &= 0 & y_1(b) &= 0 & y_3(a) &= 0 & y_3(b) &= 1
\end{align}
using the approximation $\int |\varPsi(x)|^2 dx \approx \sum \omega
\varPsi^2(x)$.  This formulation needs fixed boundaries for
integration ($a$ - left boundary, $b$ - right boundary).  A
formulation with a free boundary at the right side would include the
introduction of another variable $t y_{4} = x-a$, where $a$ is the
left boundary and $t$ is the new integration variable (integrated from
0 to 1).  This transformation was not performed in the implementation,
as an additional boundary condition would have been need and no
suitable was found.

The method of choice to solve one-dimensional boundary value problems
is the \textit{shooting method}.  In it \eqref{eq:23} is solved as a
\textit{initial value problem} with an integrator starting from one
side with a guess $s$.  The derivation from the targeted boundary
condition are regarded as none-linear equation system for matching the
boundary conditions \eqref{eq:28} $\tilde s$.  This is solved
iteratively with a \textit{equation system solver}.  In the following
subsection shall be introduced the used integrator and equation system
solver first and then the implemented shooting method.

\subsection{Integrator}
\label{sec:integrator}

A first order initial value problem is usually formulated as
\begin{align}
  \label{eq:30}
  y' &= f(x,y) & x(a) &= x_0 & y(a) &= y_0.
\end{align}
To get a approximation $\eta(b)$ for $y(b)$ the system is numerically
integrated using a so-called \textit{integrator} from the start value
at $x = a$ to the desired end value at $x = b$.  The integration
happens piece-wise and is approximated by a sum of intervals.  A
commonly used example is the \textit{4th order Runge-Kutta} algorithm:
\begin{subequations}
  \label{eq:9}
  \begin{align}
    \intertext{Coefficients:}
    \label{eq:2}
    k_1 &= f\left(x_{i}, \eta_{i} \right) \\
    \label{eq:3}
    k_2 &= f\left(x_{i} + \frac{h}{2}, \eta_{i} + \frac{h}{2} k_1\right) \\
    \label{eq:4}
    k_3 &= f\left(x_{i} + \frac{h}{2}, \eta_{i} + \frac{h}{2} k_2\right) \\
    \label{eq:5}
    k_4 &= f\left(x_{i} + h, \eta_{i} + h k_3\right) \\
    \intertext{Integration:}
    \label{eq:6}
    x_{i+1} &= x_i + h \\
    \label{eq:7}
    \eta_{i+1} &= \eta_i + \frac{h}{6}\left(k_1 + 2 \left(k_2 +
        k_3\right) + k_4\right)
  \end{align}
\end{subequations}

The function $f(x,y)$ is integrate with the constant step-size $h$ and
$\eta_{i+1}$ is an approximation of $y$ at $x_{i+1}$.  Through the
intermediate steps, higher derivatives are included in the solution
(in this case up to the fourth order) reducing derivation from the
exact value.  While a small steps-size $h$ reduces this error, more
function evaluation are needed causing longer computation times this
way.  Therefore one tries to have a as large as possible $h$-value
while trying to stay below a certain threshold for the error.
Commonly, to have a changing step-size during the calculation, the
results of two integrators are compared and depending on their
difference the step-size is changed.

Additionally the problem of \textit{stiffness} exists for the
investigated systems.  Such cases are e.g. the integration of an
exponential decline, which is a rather simple function and one would
assume rather large step-sizes could be used when integrating the
tail.  This holds only true for very specific algorithms and
unfortunately not for the presented algorithm~\eqref{eq:9}.  The
solution for the Schroedinger equation has to decline exponentially in
classically forbidden areas and will hence act stiff.

A suitable integrator are \textit{Predictor-corrector algorithms}.
While they can allow step-size control, they do not enforce a specific
constant grid.  The chosen algorithm is presented in the following.
The corrector steps are:
\begin{subequations}
  \label{eq:12}
  \begin{align}
    \label{eq:10}
    \eta^{(1)}_{i+1} &= \eta_{i} + \frac{h_i}{2}
    \left(f\left(x_{i+1},\eta_{i+1}\right) +
      f\left(x_{i},\eta_{i}\right)\right) \\
    \label{eq:11}
    \eta^{(2)}_{i+1} &= \eta_{i} + \frac{h_i}{6\left(1+s_i\right)}
    \left( \left(3 + 2 s_i\right) f\left(x_{i+1},\eta_{i+1}\right) +
      \left(3 + s_i + s_i^2\right)f\left(x_{i},\eta_{i}\right) - s_i^2
      f\left(x_{i-1},\eta_{i-1}\right) \right).
  \end{align}
\end{subequations}
Two correctors are used to control the step-size in the following way:
\begin{subequations}
  \label{eq:13}
  \begin{align}
    \label{eq:14}
    h_{i+1} &= 0.9 \cdot h_i \left( \frac{tol}{\Delta\eta_i} \right)^{\frac{1}{4}} \\
    \intertext{with $tol=1e-8$ and the safety-limit:}
    \label{eq:16}
    &0.5 h_i \leq h_{i+1} \leq 1.5 h_i \\
    \intertext{and the norm of corrector-difference}
    \label{eq:17}
    \Delta\eta_i &= \|\eta^{(1)}_{i+1} - \eta^{(2)}_{i+1} \|_2
  \end{align}
\end{subequations}
As \eqref{eq:13} are implicit algorithm, they contain in the right
hand side not only the solutions of past steps but also at that moment
computed and therefore unknown value $\eta_{i+1}$.  This value is
hence approximated by the \textit{Predictor-step} beforehand:
\begin{align}
  \label{eq:18}
  \tilde\eta_{i+1} &= \eta_{i} + h_{i}
  \left(\left(1+\frac{s_i}{2}\right) f\left(x_{i}, \eta_{i}\right) -
    \frac{s_i}{2} f\left(x_{i-1}, \eta_{i-1}\right) \right),
\end{align}
which is then used to calculate \eqref{eq:10} and \eqref{eq:11}.
$s_i$ is in all equation above the fraction of previous and current
step-size $\frac{h_i}{h_{i+1}}$.  As the predictor and the corrector
algorithms are of the multi-step type, they need more than one initial
guess $\eta_0$ at the beginning and only for the first step the
Runge-Kutta algorithm~\eqref{eq:9} is used.

\subsection{Nonlinear equation solver}
\label{sec:equation-solver}

A nonlinear equation solver tries to find a solution $\mathbf{x}$ for
a group of $n$ equation
\begin{align}
  \label{eq:32}
  \mathbf{F}(\mathbf{x}) = [f_1(\mathbf{x}), \dots, f_n(\mathbf{x})]^T
  &= 0.
\end{align}
One of the fastest converging method for such system is the
\textit{Newton's method}.  To find an appropriate $\mathbf{\tilde x}$
fulfilling \eqref{eq:32}
\begin{align}
  \label{eq:33}
  \mathbf{x}_{i+1} &= \mathbf{x}_{i} - \mathbf{DF}^{-1}(\mathbf{x_i})
  \cdot \mathbf{F}(\mathbf{x_i})
\end{align}
is iterated until convergence is reached. Here $\mathbf{DF}^{-1}$ is
the inverse of the Jacobian matrix:
\begin{align}
  \label{eq:34}
  DF_{ij}(\mathbf{x_i}) = \frac{\partial f_i}{\partial x_j}.
\end{align}
For systems normal systems often more than one solution exists.  Hence
a \eqref{eq:33} will only a subset of all starting guess will
converged to the desired solution.  This subset is often the local
environment of the targeted solution.  The nature of the method of
following the function curves/surfaces/\dots also prevents finding
solutions across singularity.  Another problem may be \textit{cycles},
where the iteration jumps between two or more $\mathbf{x}$ values
constantly without reaching a converged value.  If one is able to
detected such cycles, it can be fixed by addition of a damping factor
$\lambda \le 1$ to the subtrahend in \eqref{eq:33}.

As the computation of the inverse Jacobian matrix \eqref{eq:34} may be
computationally costly, the so-called \textit{Broyden's update} is
used.  After an initial $\mathbf{DF}^{-1} = \mathbf{B}_0$ is obtained
the iteration scheme is as follows:
\begin{subequations}
  \label{eq:37}
  \begin{align}
    \label{eq:35}
    \mathbf{x}_{i+1} &= \mathbf{x}_{i} - \mathbf{B}_i(\mathbf{x_i})
    \cdot \mathbf{F}(\mathbf{x_i}) \\
    \label{eq:36}
    \mathbf{s}_{i+1} &= \mathbf{x}_{i+1} - \mathbf{x}_{i} \\
    \label{eq:38}
    \mathbf{y}_{i+1} &=  \mathbf{F}(\mathbf{x_{i+1}}) -  \mathbf{F}(\mathbf{x_i}) \\
    \label{eq:39}
    \mathbf{B}_{i+1} &= \mathbf{B}_{i} + \frac{ \left(\mathbf{s}_{i+1}
        - \mathbf{B}_{i} \mathbf{y}_{i+1} \right) \mathbf{s}^T_{i+1}
      \mathbf{B}_{i}}{\mathbf{s}^T_{i+1} \mathbf{B}_{i}
      \mathbf{y}_{i+1} }
  \end{align}
\end{subequations}
As the convergence of \eqref{eq:37} is slower than the ordinary
Newton's method, both are often used with in combination, where each
Newton's step some Broyden steps follow.


\subsection{Shooting method}
\label{sec:shooting-method}

To solve boundary value problems between two points $a$ and $b$, the
\textit{shooting methods} are the method of choice.  The equation to
solve is with $\mathbf{y}, \mathbf{f}, \mathbf{r} \in \mathbb{R}^n$
\begin{subequations}
  \label{eq:42}
  \begin{align}
    \label{eq:40}
    \mathbf{y}'(x) &= \mathbf{f}(x,\mathbf{y}(x)) \\
    \intertext{for a given $f$ and the boundary
      condition}\label{eq:41} \mathbf{r}(\mathbf{y}(a),\mathbf{y}(b))
    &= 0.
  \end{align}
\end{subequations}
As mentioned above boundary value problems [BVP] are reduce to initial
value problems [IVP].  With a guess start value
\begin{subequations}
  \label{eq:46}
  \begin{align}
    \label{eq:43}
    \mathbf{s} &= [\sigma_0, \dots , \sigma_{n-1} ]^T\\
    \intertext{the IVP is}
    \label{eq:44}
    \mathbf{y}'(x) &= \mathbf{f}(x,\mathbf{y}(x)); ~~ \mathbf{y}(a) =
    \mathbf{s}
  \end{align}
\end{subequations}

For arbitrary $\mathbf{s}$ \eqref{eq:44} will not match the boundary
conditions \eqref{eq:41} when solved with an integrator like described
in \ref{sec:integrator}.  With the Newton iteration from previous
section
\begin{subequations}
  \label{eq:47}
  \begin{align}
    \label{eq:48}
    \mathbf{s}_{i+1} &= \mathbf{s}_i - \mathbf{DF}^{-1}(\mathbf{s}_i) \cdot \mathbf{F}(\mathbf{s}_i) \\
    \intertext{the $\mathbf{s^\ast}$ is searched fulfilling}
    \label{eq:49}
    \mathbf{F}(\mathbf{s^\ast}) &= 0 \text{ with }
    \mathbf{F}(\mathbf{s}_i) :=
    \mathbf{r}(\mathbf{y}(a),\mathbf{y}(b)) = \mathbf{r}(\mathbf{s},\mathbf{y}(b,\mathbf{s})). \\
    \intertext{The Jacobian matrix \eqref{eq:34} is}
    \label{eq:50}
    \mathbf{DF}(\mathbf{s}_i) &= \left[ \frac{ \partial
        F_j(\mathbf{s})}{\partial \sigma_k}\right]_{j,k} \\
    \intertext{ but is often not analytically available and hence
      approximated with}
    \label{eq:51}
    \frac{\partial F_j(\mathbf{s})}{\partial \sigma_k} &\approx
    \frac{F_j(\sigma_0, \dots, \sigma_k + \delta\sigma_k, \dots ,
      \sigma_{n-1}) - F_j(\sigma_0, \dots , \sigma_{n-1})}{
      \delta\sigma_k}.
  \end{align}
\end{subequations}

\begin{figure}[tb]
  \centering \input{../plots/test1}
  \caption{Test system \eqref{eq:52} solved by the current
    implementation. As a starting guess are chosen $ \mathbf{\tilde
      s}_0 = [-10, 4]$ and $\mathbf{\tilde s}_1 = [-50, 4]$ and
    convergence is reached after 7 and 6 iterations using Broyden's
    update after a initial Newton step.  The plot shows how $y_1(x)$
    evolves until convergence is reached.}
  \label{fig:test1}
\end{figure}

This method is called \textbf{single shooting method}[SSM] and a
simple case of implementation is the first implementation validation
test done by the code:
\begin{subequations}
  \label{eq:52}
  \begin{align}
    \label{eq:53}
    \mathbf{y} ' &=
    \begin{pmatrix*}[l]
      \frac{3}{2} y_1^2 \\
      y_0
    \end{pmatrix*} & \mathbf{F}(\mathbf{s}) &=
    \begin{pmatrix*}[l]
      \sigma_1 - 4 \\
      (\mathbf{y}(b,\mathbf{s}))_1 - 1
    \end{pmatrix*} \\
    \intertext{where $\mathbf{y}(b,\mathbf{s})$ is the solution of the
      IVP at $b$ for $\mathbf{s}$.  This system has two solutions (see
      also Fig.~\ref{fig:test1})}
    \label{eq:54}
    \mathbf{s}_0 &= [-8,4]^T & \mathbf{s}_1 &= [-35.8585,4]^T
  \end{align}
\end{subequations}

The single shooting method has several problems:
\begin{itemize}
\item for some systems small perturbation of the starting guess for
  $\mathbf{s}$ may have an huge influence the solution.  In the worst
  case scenario this will prevent convergence.
\item additionally the convergence of Newton's method as mentioned in
  Sec.~\ref{sec:equation-solver} is only given in a local environment
  around the solution.
\item in some systems may exist singularities, which depend on
  $\mathbf{s}$.  For some starting guess the integrator will never
  reach the right side, preventing a solution.
\end{itemize}

All this problems are targeted by the \textbf{multiple shooting
  method}[MSM]: The whole interval $[a,b]$ is splitted into $m$ parts
and on each small interval a initial value problem is computed and
then the local interval parameters are modified to obtain one smooth
solution.  As the evaluation are only performed on small intervals
perturbation have a smaller impact on the solution on the right side
of interval.  Also Newton's method is more likely to find a solution
in a small region and as will be shown below singularities can be
intercepted.

With the vector $\mathbf{s} = [\mathbf{s}_0, \dots, \mathbf{s}_{m-1}]
\in \mathbb{R}^{m \cdot n}$ the solution will have the form
\begin{subequations}
  \label{eq:8}
  \begin{align}
    \label{eq:15}
    \mathbf{y} &= \left\{
      \begin{array}{l l}
        \mathbf{y}(x;x_k,\mathbf{s}_k) & \quad \text{if } x \in [x_k,
        x_{k+1})\\
        \mathbf{s}_{m-1} & \quad  \text{if } x = x_m = b
      \end{array}
    \right. \\
    \intertext{matching at each interval boundary leads to nonlinear
      equation system}\label{eq:29} \mathbf{F}(\mathbf{s}) &=
    \begin{pmatrix}
      \mathbf{y}(x_1;x_0,\mathbf{s}_0) - \mathbf{s}_1 \\
      \vdots \\
      \mathbf{y}(x_{m-1};x_{m-2},\mathbf{s}_{m-2}) -
      \mathbf{s}_{m-1} \\
      \mathbf{r(\mathbf{s}_0, \mathbf{s}_{m-1})}
    \end{pmatrix} = 0 \\
    \intertext{and for the Newton iteration the Jacobian matrix}
    \label{eq:31}
    \mathbf{DF}(\mathbf{s}) &=
    \begin{pmatrix}
      \mathbf{Y}_0 & - \mathbb{1} & 0 & \cdots& 0 \\
      0   & \mathbf{Y}_1 & - \mathbb{1} & \ddots  &  \vdots \\
      \vdots&\ddots& \ddots  &  \ddots &  0 \\
      0   &\cdots&       0 & \mathbf{Y}_{m-2} & - \mathbb{1} \\
      \mathbf{A} & 0 & \cdots & 0 & \mathbf{B}
    \end{pmatrix} \\
    \intertext{where $\mathbf{Y}_i$ are the Jacobians according to
      \eqref{eq:50} or \eqref{eq:51}. $\mathbf{A}$ and $\mathbf{B}$
      are the Jacobian from the of the start and end values:}
    \label{eq:45} &
    \begin{matrix*}[l]
      A &= D_{\mathbf{s}_0}
      \mathbf{r}(\mathbf{s}_0,\mathbf{s}_{m-1})\\
      B &= D_{\mathbf{s}_{m-1}}
      \mathbf{r}(\mathbf{s}_0,\mathbf{s}_{m-1})
    \end{matrix*}
  \end{align}
\end{subequations}

This method was also implemented and used to implicitly perform the
matching of wavefunctions and their derivatives at a special point
like requested by the problem.  The second test checks, if the
multiple shooting method is working as expected by solving the system
\begin{align}
  \label{eq:55}
  \mathbf{y}'(x,\mathbf{y}) &=
  \begin{pmatrix*}
    -\exp{(-x \cdot y_1)} - \sin{y_0} \\
    y_0
  \end{pmatrix*} & \mathbf{F}(\mathbf{s}) &=
  \begin{pmatrix*}
    (\mathbf{y}(a,\mathbf{s}))_1\\
    (\mathbf{y}(b,\mathbf{s}))_1
  \end{pmatrix*}.
\end{align}
This system has only one solution as shown in Fig.~\ref{fig:test2}
\begin{align}
  \label{eq:56}
  \mathbf{s} = [ 0.521707, 0 ]^T
\end{align}
\begin{figure}[htb]
  \centering \input{../plots/test2}
  \caption{Showing $y_1$ of \eqref{eq:55} solved with the current
    implementation. One can clearly distinguish between the different
    intervals fused by the algorithm.  The interval size is $\Delta
    x_k = 0.2$.  As a starting guess was chosen $\mathbf{s}_k = [0,
    0.2]^T$ for each interval.}
  \label{fig:test2}
\end{figure}

Despite taking a fixed interval length one problem for the multiple
shooting method is an intelligent selection of nodes to form
intervals.  Hereby as each node points increases the computational
effort (especially the calculation of \eqref{eq:31} is costly) it is
tried to have as few as possible points.  But just increasing the
interval length may prevent a proper resolution of singularities.  A
common technique for this problem is the use of an \textbf{estimator
  function}.  During the first iteration a starting guess is taken
from a simple function $\mathbf{\eta}^\ast(x)$ and the solution is
integrated until the derivation at a point $\tilde x$ the derivation
between the current solution and $\mathbf{\eta}^\ast(x)$ is greater
than a defined limit.  Then a new node is placed at the next interval
begins again with the estimator values as a starting guess for the
initial value problem.  This way singularities can be intercept as
shown in Fig.~\ref{fig:test3} where the iteration to the solution of
the system:
\begin{align}
  \label{eq:57}
  \mathbf{y}'(x,\mathbf{y}) &=
  \begin{pmatrix*}
    5 \sinh{(5 \cdot y_1)} \\
    y_0
  \end{pmatrix*} & \mathbf{F}(\mathbf{s}) &=
  \begin{pmatrix*}
    (\mathbf{y}(a,\mathbf{s}))_1\\
    (\mathbf{y}(b,\mathbf{s}))_1 - 1
  \end{pmatrix*}. \\
  \intertext{the used estimator is}
  \label{eq:58}
  \mathbf{\eta}^x(x) &=
  \begin{pmatrix*}
    1\\
    x
  \end{pmatrix*} \\
  \intertext{and a solution is}
  \label{eq:59}
  \mathbf{s} &= [ 0.0457505, 0 ]^T.
\end{align}
Infect it can be shown analytically that in the current interval only
starting guesses for $\sigma_0 \leq 0.05$ do not lead to
singularities.  Without knowing the solution or using a the multiple
shooting method a lot guess-work would have to be made to obtain a
solution.

\begin{figure}[htb]
  \centering \input{../plots/test3}
  \caption{Solving the third test \eqref{eq:57}. Plotted is $y_1$ and
    the nodes are placed either after a maximum distance of $\Delta x
    = 0.2$ or if the relative error $\frac{\| \mathbf{\eta}(x) -
      \mathbf{\eta}^\ast(x)\|_2}{\| \mathbf{\eta}^\ast(x)\|_2} \geq 0.2$}
  \label{fig:test3}
\end{figure}

\section{Implementation details}
\label{sec:impl-deta}

In the following section a brief introduction to the source code is
given about the implementation of the methods described in the
previous section.  Generally the program does not contain much
interactivity and is only controlled by command-line parameter.
Called only with a number {2,3,4} will perform the execution of the
task b,c and d, whose results are detailed in Sec.~\ref{sec:tasks}.
Calling the program with '-t' and a number thereafter will perform the
implementation tests listed in \ref{sec:impl-deta} or as examples from
\ref{sec:shooting-method}.  If the program is called without any
option the example function ``make\_run()'' from ``main.cpp'' is
executed.  This function could be a good start for either own
extension of the source code or deeper implementation knowledge as it
is extensively commented.  Beside these few default cases, any system
changes are needing a recompilation of the source code.

The program itself is structured in the following way:
\begin{enumerate}
\item Global functions are implemented in the three files
  \begin{itemize}
  \item ``main.cpp'' - which contains, beside the main-function and
    input handling, an example case (electron in a parabola shaped
    potential)
  \item ``method\_tests.cpp'' contains all test specified in
    \ref{sec:implementaion-tests}
  \item ``tasks.cpp'' contains the given problems to solve
  \end{itemize}
\item Additionally the program has three classes:
  \begin{itemize}
  \item ``FUNCTIONAL'' is the main storage class.  It also contains
    the equation solver routines, the integrator, handles the
    file-output and function-pointers to the investigated system.  Its
    definition is in the header-file ``functional.hpp'':
    \begin{itemize}
    \item ``solve\_les.cpp'' contains the functions performing the
      shooting and formulating the equation system this way.  It also
      contains the function performing the Newton-Iteration or
      Broyden's update.  The matrix inversion is done by calling two
      LAPACK-functions (``get\_inverse()''): first a LU-decomposition
      and afterwards a triangulation-function solving for the inverse.
    \item ``ode\_solve.cpp'' is a implementation of the integration
      algorithm described in Sec.~\ref{sec:integrator}.
    \item ``functional\_print.cpp'' handles the file- and screen
      print-out.
    \item ``functional\_eq\_sys\_handle.cpp'' manages the
      function-pointer to the computed system.
    \end{itemize}
  \item ``DEFAULT\_SYSTEM'' provides the one dimensional Schrödinger
    equation in the formulation of~\eqref{eq:23} with its boundary
    conditions \eqref{eq:28}.  Although it has some default potentials
    the used potential is thereby not hard implemented, but can rather
    be set through function-pointers (source code in
    ``default\_system.cpp'').
  \item ``ESTIMATOR'' is a nested class of ``FUNCTIONAL''.  It
    provides different estimator functions, which can be used to
    generate additional nodes for the multiple shooting method on the
    fly during computation.  Its source is located in
    ``estimator.cpp''.
  \end{itemize}
\item the header-file ``vector-operations.hpp'' contained all
  overloaded operators used to processed the used std-vectors.  Hereby
  also matrices are stored as one dimensional vectors.  This is needed
  as the FORTRAN LAPACK routines used for the matrix inversion can
  only process such objects.
\item the C++-code generates converged solution - an analyze of the
  results takes only to a very limited amount place.  This work is mainly
  done by the GNUPLOT scripts ``plots/task\_*.glt'' and
  ``plots/plot\_tests.glt''
\item the ``Makefile'' should build the C++-code.
\end{enumerate}

For additional details please see the \textit{``README''}-file and the
source-code itself located in \textit{'code'}.

\section{Tasks}
\label{sec:tasks}

All in all four task were to work on:
\begin{itemize}
\item the implementation of a shooting algorithm
\item the comparison of analytical and numerical results of
one-dimensional Schrödinger equation~\eqref{eq:23} for systems with a
known solution
\item a specific given of a triangular potential
\item and the investigation of square-root potential in front of an
  infinite wall.
\end{itemize}

\subsection{Implementation tests}
\label{sec:implementaion-tests}

To validate a proper implementation five test can be performed by the
program.  Three of its results were already shown as examples in
Sec.~\ref{sec:shooting-method}:
\begin{itemize}
\item Fig.~\ref{fig:test1} testing the \textbf{single shooting method}
\item Fig.~\ref{fig:test2} is a result obtained using the
  \textbf{multiple shooting method}
\item Fig.~\ref{fig:test3} shows the \textbf{on-the-fly interval generation} for
  a system tending toward having singularities.  
\end{itemize}

The fourth test proves that, if parts of the solution are already known
or can be sufficiently good guessed, the estimator function can also
be used to enforce a certain solution.  The system is
\begin{align}
  \label{eq:60}
  \mathbf{y}'(x,\mathbf{y}) &=
  \begin{pmatrix*}
    -y_2 \cdot y_1 \\
    y_0 \\
    0 \\
    y_1^2
  \end{pmatrix*} & \mathbf{F}(\mathbf{s}) &=
  \begin{pmatrix*}
    (\mathbf{y}(a,\mathbf{s}))_1\\
    (\mathbf{y}(b,\mathbf{s}))_1\\
    (\mathbf{y}(a,\mathbf{s}))_3 \\
    (\mathbf{y}(b,\mathbf{s}))_3
  \end{pmatrix*}
\end{align}

\begin{figure}[htb]
  \centering \input{../plots/test4}
  \caption{Solutions to \eqref{eq:60} using \eqref{eq:63} as estimator
    functions.}
  \label{fig:test4}
\end{figure}

This is practically the one dimensional Schrödinger equation for a
electron trapped in a infinite potential well.  As a result one would
expected that solution for $y_1$ is of the form
\begin{align}
  \label{eq:63}
  \tilde y_1(x) &= sin(\pi \cdot n \cdot x) \quad n \in \mathbb{N}.
\end{align}
Without giving the specific eigenvalue $y_2$ Fig.~\ref{fig:test4}
shows that the knowledge of $y_1$ (and therefore $y_0$) is enough to
enforce a certain solution on to the system.

As estimator function not only normal functions can be used, but also
previous obtained solution, which are present as a numerically result.
If $y(x)$ is needed, the initial value problem is solved again until
$x$ starting with the already obtained interval solution
$\mathbf{s}_k$.  This feature can also combine two solution and is
tested in the last, fifth test: solving the Schrödinger equation for
an infinite well with a deform bottom to find the ground-state:
\begin{align}
  \label{eq:64}
  V(x) &= \left\{
      \begin{array}{r l}
        \infty, & \quad  x < 0.0 \\
        -2.0, & \quad  0.0 \leq x \leq 1.0 \\
        0.0, & \quad  1.0 < x < 3.0 \\
        -2.0, & \quad  3.0 \leq x \leq 4.0 \\
        \infty, & \quad  x > 4.0 \\
      \end{array}
    \right.
\end{align}
At first the ground state is obtained, where \eqref{eq:64} is
modified to $V(x) = 0.0, 3.0 \leq x \leq 4.0$.  Then this solution
is combined symmetrically and anti-symmetrically and both
possibilities are used as estimator function for the full problem:
\begin{subequations}
  \label{eq:65}
  \begin{align}
    \label{eq:66}
    \varPsi^\ast_s(x) =  \frac{1}{2}*(\tilde\varPsi(4-x) + \tilde\varPsi(x)) \\
    \label{eq:67}
    \varPsi^\ast_a(x) =  \frac{1}{2}*(\tilde\varPsi(4-x) - \tilde\varPsi(x))
  \end{align}
\end{subequations}
As can be seen in Fig.~\ref{fig:test5} both guesses are quite close to
their final solution.  

\begin{figure}[htb]
  \centering
  \input{../plots/test5}
  \caption{Results of the fifth test.  It shows the ability to combine
    previous solution to be used as estimator functions for the
    following calculations.  The solution are shifted by their energy
    eigenvalue and the result $\varPsi_s(x)$ from the symmetric
    combination $\varPsi^\ast_s(x)$ has the lowest of all }
  \label{fig:test5}
\end{figure}

\subsection{Potentials with anlyical answer}
\label{sec:potent-with-anly}

Next the solutions for two analytic cases are presented.  At first the
results for an infinite wall system with the potential
\begin{align}
  \label{eq:68}
  V(x) &= \left\{
      \begin{array}{r l}
        \infty, & \quad  x < 0.0 \\
        0.0, & \quad  0.0 \leq x \leq 1.0 \\
        \infty, & \quad  x > 1.0 \\
      \end{array}
    \right.
\end{align}
The analytic solutions are
\begin{align}
  \label{eq:69}
  \varPsi_n(x) &= \sqrt{2} \cdot sin(\pi \cdot n \cdot x), \quad n \in \mathbb{N} \\
  \intertext{with the energy eigenvalues}
  \label{eq:70}
  E_n &= \frac{ (n\cdot \pi)^2}{2}. 
\end{align}
As can be seen in Fig.~\ref{fig:task2_a1} the results matches
perfectly.  Not shown, but the agreement is also within $\Delta E \leq
1e-6$ (see ``plots/task2\_a-diff.pdf'').

\begin{figure}[htb]
  \centering
  \input{../plots/task2_a1}
  \caption{Analytic and numerical results for the infinite wall potential \eqref{eq:68}.}
  \label{fig:task2_a1}
\end{figure}

The second case is a harmonic potential with a frequency $\omega = 1.0
~\si{\hartree \per \planckbar}$ for an electron:
\begin{align}
  \label{eq:71}
  V(x) &= \frac{x^2}{2} \\
  \intertext{The analytic solutions are}
  \label{eq:72}
  \varPsi_n(x) &= \frac{1}{\sqrt{2^n \cdot n!} \cdot \pi^{\frac{1}{4}}}
  \cdot \exp{\left( - \frac{x^2}{2}\right)} \cdot H_n(x) \\
  \intertext{with the Hermitian polynomials $H_n(x)$. The energy
    eigenvalues are }
  \label{eq:73}
  E_n &= n + \frac{1}{2}.
\end{align}
While the energy values again matches within the numerical accuracy of
the output,  the difference between \eqref{eq:72} and the numerical
results are larger $|\Delta \varPsi(x)| \leq 3e-4$ as shown in
Fig.~\ref{fig:task2_b2}.  Thereby is the greatest error in the
regions, where the wavefunctions becomes zero and the numerical
solution is approaching zero too fast.  This may be caused due to too
small interval boundaries, which are only extended until a convergence
for the energy eigenvalue is reached.  For this problem two additional
plots are locate in ``plots'': ``task2\_b-comparison.pdf'' and
``task2\_b-in-potential.pdf''.   They show like Fig.~\ref{fig:task2_a1}
the good agreement of numerical and analytic solution, but do not
provide any additional insights. 
\begin{figure}[htb]
  \centering
  \input{../plots/task2_b2}
  \caption{Difference between analytic and numerical solution for a
    harmonic potential \eqref{eq:71}.  The errors follow interestingly
  the curve of the next higher order solution.  Also for curves with
  a low number of intervals, the interval boundary are clearly to be
  distinguished.}
  \label{fig:task2_b2}
\end{figure}

\subsection{Triangular potential}
\label{sec:triangular-potential}

The third problem is to find the solutions for the triangular
potential
\begin{align}
  \label{eq:74}
  V(x) &= \left\{
    \begin{array}{r l}
      \infty, & \quad  x \leq 0.0 \\
      0.023 \si{\eV \per \nm} \cdot x, & \quad x > 0
    \end{array}.
  \right.
\end{align}
As all computation are done using atomic units, instead the system
\begin{align}
  \label{eq:78}
  V(x) &= \left\{
    \begin{array}{r l}
      \infty, & \quad  x \leq 0.0 \\
      \lambda \cdot x, & \quad x > 0
    \end{array}.
  \right.
\end{align}
was investigated with $\lambda = 1.0 \si{\hartree \per \bohr}$ to find
different states.  Afterwards $a$ was scaled to its influence on the
ground-state.  Its solution wavefunction are plotted in
Fig.~\ref{fig:task3_phi} and they are as expected with a oscillating
part in regions classically allowed an a exponential declining
part in the forbidden area.  The analysis of the energy eigenvalues
will be done in the following section together with the results
obtained for a $\sqrt{x}$-potential in front of the infinite wall.  

\begin{figure}[htb]
  \centering \input{../plots/task3_phi}
  \caption{Wavefunctions for the different solutions of \eqref{eq:78}}
  \label{fig:task3_phi}
\end{figure}

The Schrödinger equation for a triangular potential has analytical
solutions defined by the so-called \textit{Airy functions}.  According
to this solution the energy eigenvalues are determined by
\begin{subequations}
  \label{eq:75}
  \begin{align}
    \label{eq:76}
    E_n & = \left( \frac{\lambda^2}{2} \right)^{\frac{1}{3}} \cdot a(n). \\
    \intertext{For the $a(n)$ an approximation formula exists:}
    \label{eq:77}
    a(n) & = \left( \frac{3 \pi}{2} \left(n - \frac{1}{4} \right) \right)^{\frac{2}{3}}.
  \end{align}
\end{subequations}

\begin{figure}[htb]
  \centering \input{../plots/task4_phi}
  \caption{Wavefunctions for the different solutions of \eqref{eq:79}}
  \label{fig:task4_phi}
\end{figure}

\subsection{$\sqrt{x}$-potential}
\label{sec:sqrtx-potential}

The last task is it to compare the solutions with a potential
\begin{align}
  \label{eq:79}
  V(x) &= \left\{
    \begin{array}{r l}
      \infty, & \quad  x \leq 0.0 \\
      \lambda \cdot \sqrt{x} + \gamma, & \quad x > 0
    \end{array}.
  \right.
\end{align}
with the one of \eqref{eq:78}.  It is assumed that $\gamma \neq 0$
will only be addition to or from the eigenvalues and other cases than
$\gamma=0$ are not investigated.  The solutions for $\lambda = 1
\si{\hartree \bohr}^{-\frac{1}{2}}$ are shown in
Fig.~\ref{fig:task4_phi} and are similar to Fig.~\ref{fig:task3_phi}.

In Fig.~\ref{fig:task34_energy_n} the energy eigenvalues for the
current and previous case \eqref{eq:78} are plotted for the different
solutions, while $\lambda$ remains constant.  The line for
\eqref{eq:78} are the energy eigenvalue calculated according to
\eqref{eq:76}, which is matching the numerical calculated values
rather well.

Derived/guessed from \eqref{eq:76} is a single parameter ($d$) fit to
match the energy eigenvalues of \eqref{eq:79}
\begin{align}
  \label{eq:61}
  f(x) &= \left( \frac{\lambda^4}{2} \right)^{\frac{1}{5}} \cdot
  \left( \pi \cdot d \left(n - \frac{1}{4} \right)
  \right)^{\frac{2}{5}}.
\end{align}
which describes it rather well.

\begin{figure}[htb]
  \centering \input{../plots/task34_energy_n}
  \caption{Energy eigenvalues of \eqref{eq:78} and \eqref{eq:79} for
    the first ten solutions.}
  \label{fig:task34_energy_n}
\end{figure}

To support the guess \eqref{eq:61} and to give the exact eigenvalue
energies for the given potential in
Sec.~\ref{sec:triangular-potential}, in both $lambda$ is also linearly
increased and the ground-state is calculated.  The results are shown
in Fig.~\ref{fig:task34_energy_l}.  Here no fit was performed, but
just the ground state from Fig.~\ref{fig:task34_energy_n} multiplied
with a appropriate factor.

\begin{figure}[htb]
  \centering \input{../plots/task34_energy_l}
  \caption{Influence of the potential scaling factor $\lambda{}$ on
    the energy eigenvalues in \eqref{eq:78} and \eqref{eq:79} for the
    ground state.}
  \label{fig:task34_energy_l}
\end{figure}

As this agreement is quite good, the eigenvalue energies from
\eqref{eq:78} are used to calculated the ones of \eqref{eq:74} with
\begin{align}
  \label{eq:62}
  \lambda &= 0.023 \si{\eV \per \nm} = 4.47e-5 ~\si{\hartree \per
    \bohr}.
\end{align}
\begin{table}[h]
  \centering
  \begin{tabular}[h]{r c c c c c c c c c c}\hline \hline
    n & 1 & 2 & 3& 4& 5& 6& 7& 8& 9& 10 \\ \hline 
    $E_n / \si{\milli\eV}$  &  21.14 &36.97 &49.93 &61.38
    &71.85 &81.60 &90.80 &99.56 &107.95 &116.02 \\ \hline \hline
  \end{tabular}
  \caption{Calculated energy eigenvalues for \eqref{eq:74}.}
  \label{tab:task3_energy}
\end{table}

\FloatBarrier
\section{Conclusion}
\label{sec:conclusion}

After a brief introduction of the Schrödinger equation this report
listed the numerical methods implement to solve the given problem.
Their location of implementation is described in the third section.
The fourth section contains the problems used to validated the current
implementation and the results obtained to solve the given tasks.  As
far as analytical solutions are presented the numerically obtained
results are in rather good agreement.

%  The implementation of
% \textit{LUA-scripts} passable as command-line parameters had to be
% dropped due to time-shortage.  Basic groundwork was done by the
% extensive use of function-pointer, which also could point to functions
% loaded from such scripts.  As a result the program would gain
% interactivity and could process arbitrary systems without recompiling
% the source code.

\end{document}


%%% Local Variables: 
%%% mode: latex
%%% LaTeX-command: "latex -shell-escape"
%%% TeX-PDF-mode: t
%%% TeX-master: t
%%% End: 
